﻿using InventoryAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly DBContext context;

        public CategoriesController(DBContext context)
        {
            this.context = context;
        }

        [Route(""), HttpGet]
        public IActionResult Index()
        {
            var categories = context.Categories.Include(p => p.Products).ToList();

            return Ok(categories);
        }

        [Route("add"), HttpPost]
        public IActionResult AddCategory([FromBody] Category category)
        {
            context.Add(category);
            context.SaveChanges();

            return Ok("Category saved successfully!");
        }

        [Route("get/{id}"), HttpGet]
        public IActionResult GetCategory(int id)
        {
            var category = context.Categories.Include(p => p.Products).Where(p => p.CategoryID.Equals(id)).FirstOrDefault();

            return Ok(category);
        }

        [Route("update"), HttpPut]
        public IActionResult UpdateCategory([FromBody] Category category)
        {
            context.Update(category);
            context.SaveChanges();

            return Ok("Category updated successfully!");
        }

        [Route("delete/{id}"), HttpDelete]
        public IActionResult DeleteCategory(int id)
        {
            var product = context.Products.Where(p => p.CategoryID.Equals(id)).FirstOrDefault();
            if (product != null)
            {
                return BadRequest("Remove all the products under this category first!");
            }

            var category = context.Categories.Find(id);
            context.Categories.Remove(category);
            context.SaveChanges();

            return Ok("Category deleted successfully!");
        }
    }
}
