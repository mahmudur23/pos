﻿using InventoryAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellsController : ControllerBase
    {
        private readonly DBContext context;

        public SellsController(DBContext context)
        {
            this.context = context;
        }

        [Route(""), HttpGet]
        public IActionResult Index()
        {

            //var sells = context.Sells.Include(c => c.Customer).Include(c => c.ProductSells).ToList();
            var sells = context.Sells.Include(c => c.Customer).Include(c => c.ProductSells).ThenInclude(p => p.Product).ThenInclude(c => c.Category).Select(s => new Sell
            {
                SellID = s.SellID,
                Inserted = s.Inserted,
                Modified = s.Modified,
                Payment = s.Payment,
                CustomerID = s.CustomerID,
                Customer = new Customer
                {
                    CustomerID = s.Customer.CustomerID,
                    CustomerName = s.Customer.CustomerName,
                    Phone = s.Customer.Phone,
                    Email = s.Customer.Email,
                    CustomerImage = s.Customer.CustomerImage,
                    PreviousDue = s.Customer.PreviousDue,
                    Paid = s.Customer.Paid
                },
                ProductSells = s.ProductSells
            }).ToList();

            return Ok(sells);
        }

        [Route("add"), HttpPost]
        public IActionResult AddSell([FromBody] Sell sell)
        {
            DateTime dateTime = DateTime.Now;
            sell.Inserted = dateTime;
            sell.Modified = dateTime;

            context.Add(sell);
            context.SaveChanges();

            var sellDetails = context.Sells.Include(c => c.Customer).Include(c => c.ProductSells).ThenInclude(p => p.Product).Where(d => d.SellID.Equals(sell.SellID)).Select(s => new Sell
            {
                SellID = s.SellID,
                Inserted = s.Inserted,
                Modified = s.Modified,
                Payment = s.Payment,
                CustomerID = s.CustomerID,
                Customer = new Customer
                {
                    CustomerID = s.Customer.CustomerID,
                    CustomerName = s.Customer.CustomerName,
                    Phone = s.Customer.Phone,
                    Email = s.Customer.Email,
                    CustomerImage = s.Customer.CustomerImage,
                    PreviousDue = s.Customer.PreviousDue,
                    Paid = s.Customer.Paid
                },
                ProductSells = s.ProductSells
            }).FirstOrDefault();

            return Ok(sellDetails);
        }

        [Route("get/{id}"), HttpGet]
        public IActionResult GetSell(int id)
        {
            var sell = context.Sells.Include(c => c.Customer).Include(c => c.ProductSells).ThenInclude(p => p.Product).Where(c => c.SellID.Equals(id)).Select(s => new Sell
            {
                SellID = s.SellID,
                Inserted = s.Inserted,
                Modified = s.Modified,
                Payment = s.Payment,
                CustomerID = s.CustomerID,
                Customer = new Customer { 
                    CustomerID = s.Customer.CustomerID,
                    CustomerName = s.Customer.CustomerName,
                    Phone = s.Customer.Phone,
                    Email = s.Customer.Email,
                    CustomerImage = s.Customer.CustomerImage,
                    PreviousDue = s.Customer.PreviousDue,
                    Paid = s.Customer.Paid
                },
                ProductSells = s.ProductSells
            }).FirstOrDefault();

            return Ok(sell);
        }

        [Route("update"), HttpPut]
        public IActionResult UpdateSell([FromBody] Sell sell)
        {
            DateTime dateTime = DateTime.Now;
            sell.Modified = dateTime;

            context.Update(sell);
            context.SaveChanges();

            return Ok("Sells updated successfully!");
        }

        [Route("delete/{id}"), HttpDelete]
        public IActionResult DeleteSell(int id)
        {
            var sell = context.Sells.Find(id);
            context.Sells.Remove(sell);
            context.SaveChanges();

            return Ok("Sells deleted successfully!");
        }

        /*Product Sells Operation*/

        [Route("product-sells")]
        public IActionResult ProductSellsList()
        {
            var productSells = context.ProductSells.ToList();

            return Ok(productSells);
        }

        [Route("add-product-sell"), HttpPost]
        public IActionResult AddProductSell([FromBody] List<ProductSell> productSells)
        {
            foreach(var productSell in productSells)
            {
                context.Add(productSell);
                context.SaveChanges();
            }

            return Ok();
        }

        [Route("update-product-sell"), HttpPut]
        public IActionResult UpdateProductSell([FromBody] List<ProductSell> productSells)
        {
            foreach (var productSell in productSells)
            {
                context.Update(productSell);
                context.SaveChanges();
            }

            return Ok();
        }

        [Route("delete-product-sell/{id}")]
        public IActionResult DeleteProductSell(int id)
        {
            var prductSell = context.ProductSells.Find(id);
            context.ProductSells.Remove(prductSell);
            context.SaveChanges();

            return Ok();
        }
    }
}
