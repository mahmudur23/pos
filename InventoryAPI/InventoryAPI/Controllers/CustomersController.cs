﻿using InventoryAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly DBContext context;

        public CustomersController(DBContext context)
        {
            this.context = context;
        }

        [Route(""), HttpGet]
        public IActionResult Index()
        {
            var customers = context.Customers.Include(s => s.Sells).ToList();

            return Ok(customers);
        }

        [Route("add"), HttpPost]
        public IActionResult AddCustomer([FromForm] Customer customer)
        {
            var customerWithEmail = context.Customers.Where(d => d.Email.Equals(customer.Email) && !d.CustomerID.Equals(customer.CustomerID)).FirstOrDefault();

            if (customerWithEmail != null)
            {
                return BadRequest("The email already exists!");
            }

            if(customer.ImageFile != null)
            {
                // upload image
                var file = customer.ImageFile;
                string ImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                string SavePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/customer-img", ImageName);
                /*using (var stream = System.IO.File.Create(SavePath))
                {
                    file.CopyTo(stream);
                }*/
                ResizeAndUpload(SavePath, file);

                customer.CustomerImage = ImageName;
                // End image upload
            }

            context.Add(customer);
            context.SaveChanges();

            return Ok("Customer saved successfully!");
        }

        [Route("get/{id}")]
        public IActionResult GetCustomer(int id)
        {
            var customer = context.Customers.Find(id);

            return Ok(customer);
        }

        [Route("update"), HttpPut]
        public IActionResult UpdateCustomer([FromForm] Customer customer)
        {
            var customerWithEmail = context.Customers.Where(d => d.Email.Equals(customer.Email) && !d.CustomerID.Equals(customer.CustomerID)).FirstOrDefault();

            if (customerWithEmail != null)
            {
                return BadRequest("The email already exists!");
            }

            if (customer.ImageFile != null)
            {
                // delete start
                if (customer.CustomerImage.Length > 0)
                {
                    string PreviousImage = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/customer-img", customer.CustomerImage);
                    var fileInfo = new System.IO.FileInfo(PreviousImage);
                    fileInfo.Delete();
                }

                // delete end

                // upload image
                var file = customer.ImageFile;
                string ImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                string SavePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/customer-img", ImageName);
                /*using (var stream = System.IO.File.Create(SavePath))
                {
                    file.CopyTo(stream);
                }*/
                ResizeAndUpload(SavePath, file);

                customer.CustomerImage = ImageName;
                // End image upload
            }
            context.Update(customer);
            context.SaveChanges();

            return Ok("Customer updated successfully!");
        }

        [Route("delete/{id}")]
        public IActionResult DeleteCustomer(int id)
        {
            var customer = context.Customers.Find(id);

            if (customer.CustomerImage != null)
            {
                string PreviousImage = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/customer-img", customer.CustomerImage);
                var fileInfo = new System.IO.FileInfo(PreviousImage);
                fileInfo.Delete();
            }

            context.Customers.Remove(customer);
            context.SaveChanges();

            return Ok("Customer deleted successfully!");
        }

        public void ResizeAndUpload(string path, IFormFile file)
        {
            Image imageFile = Image.FromStream(file.OpenReadStream(), true, true);

            int width = 600;
            int height = 600;
            double imageWidth = imageFile.Width;
            double imageHeight = imageFile.Height;
            double ratio = 0;

            if (imageWidth > imageHeight)
            {
                ratio = imageWidth / imageHeight;
                height = (int)(width / ratio);
            }
            else if (imageWidth < imageHeight)
            {
                ratio = imageHeight / imageWidth;
                width = (int)(height / ratio);
            }

            var newImage = new Bitmap(width, height);
            using (var g = Graphics.FromImage(newImage))
            {
                g.DrawImage(imageFile, 0, 0, width, height);
                newImage.Save(path);
            }
        }
    }
}
