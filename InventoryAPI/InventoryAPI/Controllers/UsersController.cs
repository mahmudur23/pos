﻿using InventoryAPI.Models;
using JWT;
using JWT.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    //[EnableCors("CorsPolicy")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IJwtAuthenticationManager jwtAuthenticationManager;
        private readonly DBContext context;

        public UsersController(IJwtAuthenticationManager jwtAuthenticationManager, DBContext context)
        {
            this.jwtAuthenticationManager = jwtAuthenticationManager;
            this.context = context;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;

            /*string token = Request.Headers["Authorization"];
            string[] subs = token.Split("Bearer");*/

            var users = context.Users.ToList();

            return Ok(users);
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] UserCred userCred)
        {
            Token tokenModel = new Token();
            DateTime dateTime = DateTime.Now;

            var user = context.Users.Where(u => u.Email.Equals(userCred.Email) && u.Password.
            Equals(Hash.CreateHash(userCred.Password))).FirstOrDefault();

            if (user != null)
            {
                var data = context.Tokens.Where(t => t.ExpiredDate < dateTime && t.UserID.Equals(user.UserID)).ToList();
                if (data != null)
                {
                    context.Tokens.RemoveRange(data);
                    context.SaveChanges();
                }
            }

            if (user == null)
            {
                return Unauthorized("Wrong email or password!");
            }
            else
            {
                var token = jwtAuthenticationManager.Authenticate(userCred.Email, userCred.Password, user.Type);

                // Inserting token into TokenModel
                tokenModel.TokenValue = token;
                tokenModel.ExpiredDate = dateTime.AddHours(1);
                tokenModel.UserID = user.UserID;
                context.Add(tokenModel);
                context.SaveChanges();

                return Ok(token);
            }
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody] User user)
        {
            var data = context.Users.Where(e => e.Email.Contains(user.Email)).FirstOrDefault();

            if (data == null)
            {
                user.Password = Hash.CreateHash(user.Password);
                context.Add(user);
                context.SaveChanges();
            } else
            {
                return BadRequest("The user already exists!");
            }

            return Ok("User registered successfully!");
        }

        [HttpPost("login")]
        public IActionResult Login()
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var email = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            var userDetails = context.Users.Where(u => u.Email.Equals(email)).Select(u => new User
            { 
                UserID = u.UserID,
                Name = u.Name,
                Email = u.Email,
                Password = u.Password,
                Phone = u.Phone,
                Gender = u.Gender,
                Address = u.Address,
                City = u.City,
                State = u.State,
                Type = u.Type,
                UserImage = u.UserImage
            }).FirstOrDefault();

            return Ok(userDetails);
        }

        [HttpPost("logout")]
        public IActionResult Logout()
        {
            string token = Request.Headers["Authorization"];
            string[] subs = token.Split(" ");
            var data = context.Tokens.Where(t => t.TokenValue.Equals(subs[1])).FirstOrDefault();
            data.BlackListed = true;
            context.Update(data);
            context.SaveChanges();

            return Ok("Logged out successfully!");
        }

        [Route("update"), HttpPut]
        public IActionResult UpdateUser([FromForm] User user)
        {

            var userWithEmail = context.Users.Where(p => p.Email.Equals(user.Email) && !p.UserID.Equals(user.UserID)).FirstOrDefault();

            if (userWithEmail != null)
            {
                return BadRequest("The email is being used by another user!");
            }

            if (user.ImageFile != null)
            {
                if (user.UserImage != null)
                {
                    // delete start
                    string PreviousImage = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/user-img", user.UserImage);
                    var fileInfo = new System.IO.FileInfo(PreviousImage);
                    fileInfo.Delete();

                    // delete end
                }

                // upload image
                var file = user.ImageFile;
                string ImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                string SavePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/user-img", ImageName);
                /*using (var stream = System.IO.File.Create(SavePath))
                {
                    file.CopyTo(stream);
                }*/
                ResizeAndUpload(SavePath, file);

                user.UserImage = ImageName;
                // End image upload
            }

            if (user.Password.Length < 15)
            {
                user.Password = Hash.CreateHash(user.Password);
            }

            context.Update(user);
            context.SaveChanges();

            return Ok("User updated successfully!");
        }

        [Route("delete/{id}"), HttpDelete]
        public IActionResult DeleteUser(int id)
        {
            var user = context.Users.Find(id);

            if (user.UserImage.Length > 0)
            {
                string PreviousImage = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/user-img", user.UserImage);
                var fileInfo = new System.IO.FileInfo(PreviousImage);
                fileInfo.Delete();
            }

            context.Users.Remove(user);
            context.SaveChanges();

            return Ok("User deleted successfully!");
        }

        [AllowAnonymous]
        [Route("email/check"), HttpPost]
        public IActionResult CheckImail([FromForm] string email)
        {
            var user = context.Users.Where(u => u.Email.Equals(email)).FirstOrDefault();
            if (user == null)
            {
                return NotFound("Email was not found!");
            }

            return Ok(user);
        }

        [AllowAnonymous]
        [Route("otp/generate"), HttpPost]
        public IActionResult GenerateOTP([FromForm] string Email)
        {
            var user = context.Users.Where(e => e.Email.Contains(Email)).FirstOrDefault();

            Random random = new Random();
            int number = random.Next(100000, 900000);

            user.OTP = number;
            context.Update(user);
            context.SaveChanges();

            return Ok("OTP has been sent to your email!");
        }

        [Route("otp/match"), HttpPost]
        public IActionResult MatchOTP(string otp, string email)
        {
            var user = context.Users.Where(u => u.Equals(email) && u.Equals(otp)).FirstOrDefault();
            if (user == null)
            {
                return NotFound();
            }

            return Ok("Email was found!");
        }

        [Route("password/reset"), HttpPost]
        public IActionResult PasswordReset(string otp, string email, string password)
        {
            var user = context.Users.Where(u => u.Equals(email) && u.Equals(otp)).FirstOrDefault();
            if (user == null)
            {
                return NotFound();
            }

            user.Password = Hash.CreateHash(password);
            user.OTP = 0;
            context.Users.Update(user);
            context.SaveChanges();

            return Ok("Password has been reset successfully!");
        }

        public void ResizeAndUpload(string path, IFormFile file)
        {
            Image imageFile = Image.FromStream(file.OpenReadStream(), true, true);

            int width = 600;
            int height = 600;
            double imageWidth = imageFile.Width;
            double imageHeight = imageFile.Height;
            double ratio = 0;

            if (imageWidth > imageHeight)
            {
                ratio = imageWidth / imageHeight;
                height = (int)(width / ratio);
            }
            else if (imageWidth < imageHeight)
            {
                ratio = imageHeight / imageWidth;
                width = (int)(height / ratio);
            }

            var newImage = new Bitmap(width, height);
            using (var g = Graphics.FromImage(newImage))
            {
                g.DrawImage(imageFile, 0, 0, width, height);
                newImage.Save(path);
            }
        }
    }
}
