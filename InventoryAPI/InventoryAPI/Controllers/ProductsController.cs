﻿using InventoryAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly DBContext context;
        private readonly IWebHostEnvironment hostEnvironment;

        public ProductsController(DBContext context, IWebHostEnvironment hostEnvironment)
        {
            this.context = context;
            this.hostEnvironment = hostEnvironment;
        }

        [Route(""), HttpGet]
        public IActionResult Index()
        {
            var data = context.Products.Include(c => c.Category).Include(c => c.Code).Include(p => p.ProductSells).Select(s => new Product { 
                ProductID = s.ProductID,
                ProductName = s.ProductName,
                CategoryID = s.CategoryID,
                Category = s.Category,
                CodeID = s.CodeID,
                Code = s.Code,
                ProductImage = s.ProductImage,
                Qunatity = s.Qunatity,
                BuyPrice = s.BuyPrice,
                SellPrice = s.SellPrice,
                ProductSells = s.ProductSells
            }).ToList();

            return Ok(data);
        }

        [Route("add"), HttpPost]
        public IActionResult AddProduct([FromForm] Product product)
        {
            var productWithCode = context.Products.Where(p => p.CodeID.Equals(product.CodeID)).FirstOrDefault();
            if (productWithCode != null)
            {
                return BadRequest("Product code is already used!");
            }

            // upload image
            if (product.ImageFile != null)
            {
                var file = product.ImageFile;
                string ImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                string SavePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img", ImageName);
                /* using (var stream = System.IO.File.Create(SavePath))
                 {
                     file.CopyTo(stream);
                 }*/
                ResizeAndUpload(SavePath, file);

                product.ProductImage = ImageName;
            }
            // End image upload


            context.Add(product);
            context.SaveChanges();

            return Ok("Product saved successfully!");
        }

        [Route("get/{id}"), HttpGet]
        public IActionResult GetProduct(int id)
        {
            var product = context.Products.Include(c => c.Category).Include(c => c.Code).Select(s => new Product
            {
                ProductID = s.ProductID,
                ProductName = s.ProductName,
                CategoryID = s.CategoryID,
                Category = s.Category,
                CodeID = s.CodeID,
                Code = s.Code,
                ProductImage = s.ProductImage,
                Qunatity = s.Qunatity,
                BuyPrice = s.BuyPrice,
                SellPrice = s.SellPrice
            }).FirstOrDefault(x => x.ProductID == id);

            return Ok(product);
        }

        [Route("update"), HttpPut]
        public IActionResult UpdateProduct([FromForm] Product product)
        {
            var productWithCode = context.Products.Where(p => p.CodeID.Equals(product.CodeID) && !p.ProductID.Equals(product.ProductID)).FirstOrDefault();
    
            if (productWithCode != null)
            {
                return BadRequest("Product code is already used!");
            }

            if (product.ImageFile != null)
            {
                // delete start
                string PreviousImage = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img", product.ProductImage);
                var fileInfo = new System.IO.FileInfo(PreviousImage);
                fileInfo.Delete();

                // delete end

                // upload image
                var file = product.ImageFile;
                string ImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                string SavePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img", ImageName);
                /*using (var stream = System.IO.File.Create(SavePath))
                {
                    file.CopyTo(stream);
                }*/
                ResizeAndUpload(SavePath, file);

                product.ProductImage = ImageName;
                // End image upload
            }

            context.Update(product);
            context.SaveChanges();

            return Ok("Product updated successfully!");
        }

        [Route("delete/{id}"), HttpDelete]
        public IActionResult DeleteProduct(int id)
        {
            var product = context.Products.Find(id);

            if (product.ProductImage != null)
            {
                string PreviousImage = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img", product.ProductImage);
                var fileInfo = new System.IO.FileInfo(PreviousImage);
                fileInfo.Delete();
            }

            context.Products.Remove(product);
            context.SaveChanges();

            return Ok("Product deleted successfully!");
        }

        [Route("delete-image"), HttpPost]
        public IActionResult DeleteProductImage()
        {
            string SavePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img", "ec8fcd34-c62c-4ceb-ac51-3f5e908e9c30.jpg");
            var fileInfo = new System.IO.FileInfo(SavePath);
            fileInfo.Delete();

            return Ok("Image deleted successfully!");
        }

        [Route("update-sell"), HttpPut]
        public IActionResult UpdateSellProduct([FromBody] ICollection<Product> products)
        {
            context.UpdateRange(products);
            context.SaveChanges();

            return Ok("Product updated!");
        }


        public void ResizeAndUpload(string path, IFormFile file)
        {
            Image imageFile = Image.FromStream(file.OpenReadStream(), true, true);

            int width = 600;
            int height = 600;
            double imageWidth = imageFile.Width;
            double imageHeight = imageFile.Height;
            double ratio = 0;

            if (imageWidth > imageHeight)
            {
                ratio = imageWidth / imageHeight;
                height = (int)(width / ratio);
            }
            else if (imageWidth < imageHeight)
            {
                ratio = imageHeight / imageWidth;
                width = (int)(height / ratio);
            }

            var newImage = new Bitmap(width, height);
            using (var g = Graphics.FromImage(newImage))
            {
                g.DrawImage(imageFile, 0, 0, width, height);
                newImage.Save(path);
            }
        }
    }
}
