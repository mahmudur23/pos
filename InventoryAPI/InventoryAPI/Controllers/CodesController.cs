﻿using InventoryAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CodesController : ControllerBase
    {
        private readonly DBContext context;

        public CodesController(DBContext context)
        {
            this.context = context;
        }

        [Route(""), HttpGet]
        public IActionResult Index()
        {
            var codes = context.Codes.Include(p => p.Products).ToList();

            return Ok(codes);
        }

        [Route("add"), HttpPost]
        public IActionResult AddCode([FromBody] Code code)
        {
            var codeWord = context.Codes.Where(c => c.CodeWord.Equals(code.CodeWord)).FirstOrDefault();
            if (codeWord != null)
            {
                return BadRequest("The code already exists!");
            }

            context.Add(code);
            context.SaveChanges();

            return Ok("Productcode saved successfully!");
        }

        [Route("get/{id}"), HttpGet]
        public IActionResult GetCode(int id)
        {
            var code = context.Codes.Include(p => p.Products).Where(c => c.CodeID.Equals(id)).FirstOrDefault();

            return Ok(code);
        }

        [Route("update"), HttpPut]
        public IActionResult UpdateCode([FromBody] Code code)
        {
            context.Update(code);
            context.SaveChanges();

            return Ok("Productcode updated successfully!");
        }

        [Route("delete/{id}"), HttpDelete]
        public IActionResult DeleteCode(int id)
        {
            var product = context.Products.Where(p => p.CodeID.Equals(id)).FirstOrDefault();
            if (product != null)
            {
                return BadRequest("Remove all the products under this code first!");
            }

            var code = context.Codes.Find(id);
            context.Codes.Remove(code);
            context.SaveChanges();

            return Ok("Productcode deleted successfully!");
        }
    }
}
