﻿using Microsoft.AspNetCore.Http;
using InventoryAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Server.Middlewares
{
    public class CustomMiddleware
    {
        private readonly RequestDelegate next;
        public CustomMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext, DBContext context)
        {
            string token = httpContext.Request.Headers["Authorization"];
            CheckToken checkToken = new CheckToken(context);
            if (token != null)
            {
                string[] subs = token.Split(" ");

                if (!checkToken.BlackListed(subs[1].ToString()))
                {
                    await next(httpContext);
                }
                else
                {
                    //await httpContext.Response.WriteAsync("Unauthorized");
                    httpContext.Response.ContentType = "application/json";
                    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    await httpContext.Response.StartAsync();
                }

            }
            else
            {
                await next(httpContext);
            }
        }
    }
}
