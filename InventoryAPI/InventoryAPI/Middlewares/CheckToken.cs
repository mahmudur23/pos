﻿using InventoryAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Middlewares
{
    public class CheckToken
    {
        private readonly DBContext context;
        public CheckToken(DBContext context)
        {
            this.context = context;
        }

        public bool BlackListed(string token)
        {
            var tokenData = context.Tokens.Where(u => u.TokenValue.Equals(token)).FirstOrDefault();
            if (tokenData.BlackListed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string TokenData(string token)
        {
            return context.Tokens.Where(u => u.TokenValue.Equals(token)).FirstOrDefault().TokenValue;
        }
    }
}
