﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InventoryAPI.Migrations
{
    public partial class ProductSell : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProductSells",
                columns: table => new
                {
                    ProductSellID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SellID = table.Column<int>(type: "INTEGER", nullable: true),
                    ProductID = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductSells", x => x.ProductSellID);
                    table.ForeignKey(
                        name: "FK_ProductSells_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductSells_Sells_SellID",
                        column: x => x.SellID,
                        principalTable: "Sells",
                        principalColumn: "SellID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductSells_ProductID",
                table: "ProductSells",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSells_SellID",
                table: "ProductSells",
                column: "SellID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductSells");
        }
    }
}
