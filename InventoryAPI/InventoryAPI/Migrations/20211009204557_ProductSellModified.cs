﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InventoryAPI.Migrations
{
    public partial class ProductSellModified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductSells_Products_ProductID",
                table: "ProductSells");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductSells_Sells_SellID",
                table: "ProductSells");

            migrationBuilder.AlterColumn<int>(
                name: "SellID",
                table: "ProductSells",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProductID",
                table: "ProductSells",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductSells_Products_ProductID",
                table: "ProductSells",
                column: "ProductID",
                principalTable: "Products",
                principalColumn: "ProductID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductSells_Sells_SellID",
                table: "ProductSells",
                column: "SellID",
                principalTable: "Sells",
                principalColumn: "SellID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductSells_Products_ProductID",
                table: "ProductSells");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductSells_Sells_SellID",
                table: "ProductSells");

            migrationBuilder.AlterColumn<int>(
                name: "SellID",
                table: "ProductSells",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "ProductID",
                table: "ProductSells",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductSells_Products_ProductID",
                table: "ProductSells",
                column: "ProductID",
                principalTable: "Products",
                principalColumn: "ProductID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductSells_Sells_SellID",
                table: "ProductSells",
                column: "SellID",
                principalTable: "Sells",
                principalColumn: "SellID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
