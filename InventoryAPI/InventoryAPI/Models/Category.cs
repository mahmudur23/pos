﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Models
{
    public class Category
    {
        [Key]
        public int CategoryID { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string CategoryName { get; set; }
        // Navigation properties
        public virtual ICollection<Product> Products { get; set; }
    }
}
