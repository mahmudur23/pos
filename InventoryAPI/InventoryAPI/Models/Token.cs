﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Models
{
    public class Token
    {
        [Key]
        public int TokenID { get; set; }
        public string TokenValue { get; set; }
        public int UserID { get; set; }
        public bool BlackListed { get; set; }
        public DateTime ExpiredDate { get; set; }

        public Token()
        {
            BlackListed = false;
        }
    }
}
