﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string ProductName { get; set; }
        public int ? CategoryID { get; set; }
        public Category Category { get; set; }
        public int ? CodeID { get; set; }
        public Code Code { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        public string ProductImage { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Qunatity { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string BuyPrice { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string SellPrice { get; set; }
        public virtual ICollection<ProductSell> ProductSells { get; set; }

        public Product()
        {
            ImageFile = null;
        }
    }
}
