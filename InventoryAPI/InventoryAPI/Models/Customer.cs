﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string CustomerName { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Phone { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Email { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        public string CustomerImage { get; set; }
        public double PreviousDue { get; set; }
        [NotMapped]
        public double Paid { get; set; }
        public virtual ICollection<Sell> Sells { get; set; }

        public Customer()
        {
            ImageFile = null;
        }
    }
}
