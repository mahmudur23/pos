﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Name { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Email { get; set; }
        public string Password { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Phone { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Gender { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Address { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string City { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string State { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Type { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        public string UserImage { get; set; }
        public int OTP { get; set; }

        public User()
        {
            Type = "Moderator";
            ImageFile = null;
        }
    }
}
