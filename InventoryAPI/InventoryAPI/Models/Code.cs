﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Models
{
    public class Code
    {
        [Key]
        public int CodeID { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string CodeWord { get; set; }
        public string CodeName { get; set; }
        // Navigation properties
        public virtual ICollection<Product> Products { get; set; }
    }
}
