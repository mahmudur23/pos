﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Models
{
    public class Sell
    {
        [Key]
        public int SellID { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Modified { get; set; }
        public double Payment { get; set; }
        public int? CustomerID { get; set; }
        public Customer Customer { get; set; }
        public virtual ICollection<ProductSell> ProductSells { get; set; }
    }
}
