﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Models
{
    public class ProductSell
    {
        [Key]
        public int ProductSellID { get; set; }
        // Navigation properties
        public int SellID { get; set; }
        public Sell Sell { get; set; }
        public int ProductID { get; set; }
        public Product Product { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Quantity { get; set; }
    }
}
