import {lazy} from "react";
import Dashboard from "./admin/Dashboard";
const ProductCode = lazy(() => import("./components/code/ProductCode"));
const AddProducts = lazy(() => import("./components/products/AddProducts"));
const SellsReport =  lazy(() => import("./components/sells-report/SellsReport"));
const Customers = lazy(() => import("./components/customers/Customers"));
const CustomerDetails = lazy(() => import("./components/customers/CustomerDetails"));
const AddCustomer =  lazy(() => import("./components/customers/AddCustomer"));

const Routes = [
    {path: "/dashboard/product/code", component1: Dashboard, component2: ProductCode},
    {path: "/dashboard/product/sells-report-report", component1: Dashboard, component2: SellsReport},
    {path: "/dashboard/product/add", component1: Dashboard, component2: AddProducts},
    {path: "/dashboard/product/edit/:id", component1: Dashboard, component2: AddProducts},
    {path: "/dashboard/customers", component1: Dashboard, component2: Customers},
    {path: "/dashboard/customer/details/:id", component1: Dashboard, component2: CustomerDetails},
    {path: "/dashboard/customer/add", component1: Dashboard, component2: AddCustomer},
    {path: "/dashboard/customer/edit/:id", component1: Dashboard, component2: AddCustomer},
];

export default Routes;
