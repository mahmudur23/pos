import "./Category.scss";
import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faTrash} from "@fortawesome/free-solid-svg-icons";
import Toast from "../toast/Toast";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import Keys from "../keys/Keys";

const Category = () => {
    const [category, setCategory] = useState("");
    const [edit, setEdit] = useState(false);
    const [url, setUrl] = useState("");
    const [id, setId] = useState(0);
    const [data, setData] = useState([]);

    const showData = () => {
        axios.get(Keys.url + "/api/categories")
            .then(res => {
                setData(res.data);
                // console.log(data);
                // console.log(res.status);
            })
            .catch(err => {
                alert("Something went wrong!")
            });
    };

    useEffect(() => {
        showData();
    }, []);

    const categoryAddOrEdit = (event) => {
        event.preventDefault();

        if (category !== "") {
            const codeData = {"categoryName": category.toString()};

            if (!edit) {
                setUrl(Keys.url + "/api/categories/add");
                axios.post(url,
                    codeData
                ).then((response) => {
                    console.log(response.data);
                    Toast.successToast(response.data);
                    showData();
                    resetForm();
                }).catch((err) => {
                    console.log(err)
                });
            } else {
                setUrl(Keys.url + "/api/categories/update");
                codeData["categoryID"] = id;
                axios.put(url,
                    codeData
                ).then((response) => {
                    console.log(response.data);
                    Toast.successToast(response.data);
                    showData();
                    cancelHandler(false);
                }).catch((err) => {
                    console.log(err)
                });
            }

        } else {
            Toast.dangerToast("The fields can not be empty!");
        }
    };

    const editHandler = (value, id) => {
        setEdit(value);
        setId(id);

        axios.get(Keys.url + "/api/categories/get/" + id)
            .then(res => {
                setCategory(res.data.categoryName);
                // console.log(res.data);
            })
            .catch(err => {
                alert("Something went wrong!")
            });
    };

    const deleteCategory = (id) => {
        axios.delete(Keys.url + `/api/categories/delete/${id}`
        ).then((response) => {
            showData();
            Toast.successToast(response.data);
        }).catch((err) => {
            if (err.response.status === 400) {
                Toast.warningToast(err.response.data);
            }
        })
    };

    const cancelHandler = (value) => {
        setEdit(value);
        resetForm();
    };

    const resetForm = () => {
        setCategory("");
    };

    return (
        <div className="category-container">
            <title>Dashboard - Category</title>
            <div className="row">
                <div className="col-lg-4 col-md-5 col-md-7 col-sm-10 col-12 mx-auto">
                    <table className="table table-bordered table-dark">
                        <thead>
                        <tr>
                            <th scope="col" className="text-center">ID</th>
                            <th scope="col" className="text-center">Category name</th>
                            <th scope="col" className="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                       {
                           data.length !== 0 ?
                               data.map((d, i) => {
                                   return (
                                       <tr key={i}>
                                           <th className="text-center align-middle" scope="row">{d.categoryID}</th>
                                           <td className="text-center align-middle">{d.categoryName}</td>
                                           <td className="text-center align-middle">
                                               <a onClick={event => editHandler(true, d.categoryID)} className="ms-2 text-warning"><FontAwesomeIcon icon={faEdit}/></a>
                                               <a onClick={event => deleteCategory(d.categoryID)} className="ms-2 text-danger"><FontAwesomeIcon icon={faTrash}/></a>
                                           </td>
                                       </tr>
                                   )
                               }) : null
                       }
                        </tbody>
                    </table>
                </div>
                <div className="col-lg-4 col-md-5 col-md-7 col-sm-10 col-12 mx-auto">
                    <form onSubmit={categoryAddOrEdit}>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Category name</label>
                            <input value={category} onChange={event => setCategory(event.target.value)}
                                type="text" placeholder="Category name" id="product-code-id" className="form-control bg-dark text-white-50"/>
                        </div>
                        <input type="submit" className={`btn form-control ${!edit ? "btn-outline-primary" : "btn-outline-success"}`}
                               value={!edit ? "Add" : "Update"}/>
                        {
                            edit ? <input type={"button"} onClick={event => cancelHandler(false)} className="btn btn-outline-danger mt-2 form-control"
                                          value="Cancel"/>
                                : null
                        }
                    </form>
                </div>
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default Category;
