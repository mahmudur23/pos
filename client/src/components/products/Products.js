import "./Products.scss";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Toast from "../toast/Toast";
import React, {useState, useEffect} from 'react';
import {Link} from "react-router-dom";
import axios from "axios";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faEdit,
    faTrash
} from "@fortawesome/free-solid-svg-icons";
import {
    Pagination
} from '@material-ui/lab';
import Keys from "../keys/Keys";
import {useDispatch, useSelector} from "react-redux";
import {pageAction} from "../../actions";

const Products = () => {
    const [data, setData] = useState([]);
    const paginationPerPage = 5;
    const [pagination, setPagination] = useState([]);
    const [page, setPage] = useState(1);
    const [search, setSearch] = useState();
    const [filter, setFilter] = useState([]);

    const showData = () => {
        axios.get(Keys.url + "/api/products")
            .then(res => {
                let qty = 0;
                let profit = 0;
                if (res.data.length > 0) {
                    for (let i = 0; i < res.data.length; i++) {
                        if (res.data[i].productSells.length > 0) {
                            res.data[i].productSells.forEach(p => {
                                qty += parseInt(p.quantity);
                            })
                        }
                        res.data[i].totalSell = qty;
                        res.data[i].profit = (parseInt(res.data[i].sellPrice) - parseInt(res.data[i].buyPrice)) * qty;
                        qty = 0;
                    }
                }
                setData(res.data);
                // console.log(data);
                // console.log(res.status);
            })
            .catch(err => {
                alert("Something went wrong!")
            });
    };

    useEffect(() => {
        showData();
    }, []);

    const searchData = (event) => {
        setSearch(event.target.value);
    };

    // search
    useEffect(() => {
        setPage(1);
        let row = [];
        if (data.length !== 0) {
            data.filter((val) => {
                if (search === "" || search == null) {
                    row.push(val);
                    setFilter(row);
                } else if (val.productName.toLowerCase().includes(search.toLowerCase()) || val.category.categoryName.toLowerCase().includes(search.toLowerCase())) {
                    row.push(val);
                    setFilter(row);
                    // console.log(row);
                }
                return val;
            });
        }
    }, [search, data]);

    // pagination
    useEffect(() => {
        let temp = [];
        let tempArr = [];
        let paginationLength;
        let num = 0;
        if (filter.length != null) {
            paginationLength = Math.ceil(filter.length / paginationPerPage);
            console.log(paginationLength);

            for (let i = 1; i <= paginationLength; i++) {
                for (let j = 0; j < paginationPerPage; j++) {
                    /*if (data[num] !== undefined) {
                        tempArr.push(data[num]);
                    }*/
                    if (filter[num] === undefined) {
                        break;
                    }
                    tempArr.push(filter[num]);
                    num++;
                    if (tempArr.length === paginationPerPage) {
                        break;
                    }
                }
                temp[i] = tempArr;
                tempArr = [];
            }
            setPagination(temp);
        }
        console.log(pagination);
        console.log(pagination.length);
    }, [filter]);

    const pageChange = (event, value) => {
        setPage(value);
        // console.log(value)
        dispatch(pageAction(value));
    };

    const deleteProduct = (id) => {
        axios.delete(Keys.url + `/api/products/delete/${id}`
        ).then((response) => {
            showData();
            Toast.successToast(response.data);
        }).catch((err) => {
            console.log(err)
        })
    };

    const dispatch = useDispatch();
    const pageValue = useSelector(state => state.Page);

    useEffect(() => {
        if ((pagination.length - 1) > 0 && pageValue !== 0) {
            if (pageValue <= pagination.length - 1) {
                setPage(pageValue);
            } else if (pageValue > pagination.length - 1) {
                setPage(pagination.length - 1);
                dispatch(pageAction(pagination.length - 1));
            }
            // console.log("Page value: " + pageValue);
        }
    }, [pagination]);

    return (
        <div className="products-container">
            <title>Dashboard - All Products</title>
            <div className="row mb-1">
                <div className="col-md-4 col-sm-5 col-12 pb-1">
                    <Link to="/dashboard/product/add" className="btn btn-outline-success btn-sm ">Add Product</Link>
                </div>
                <div className="col-md-4 col-sm-5 col-12 ms-auto">
                    <div className="form-group">
                        <input type="text" onChange={event => searchData(event)}
                               placeholder="Search" name="search"
                               className="form-control bg-dark shadow-none text-white"/>
                    </div>
                </div>
            </div>
            <div className="table-container">
                <table className="table table-bordered table-dark">
                    <thead>
                    <tr className="">
                        <th scope="col" className="text-center">Product name</th>
                        <th scope="col" className="text-center">Category</th>
                        <th scope="col" className="text-center">Code</th>
                        <th scope="col" className="text-center">Picture</th>
                        <th scope="col" className="text-center">Qty</th>
                        <th scope="col" className="text-center">Buy price</th>
                        <th scope="col" className="text-center">Sell price</th>
                        <th scope="col" className="text-center">Sells</th>
                        <th scope="col" className="text-center">Total profit</th>
                        <th scope="col" className="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {/*<tr>
                        <th className="text-center align-middle" scope="row">T-Shirt</th>
                        <td className="text-center align-middle">Cloths</td>
                        <td className="text-center align-middle">64734</td>
                        <td  className="product-picture"><img src={imageFive} alt=""/></td>
                        <td className="text-center align-middle">100</td>
                        <td className="text-center align-middle">2000</td>
                        <td className="text-center align-middle">2200</td>
                        <td className="text-center align-middle">10000</td>
                        <td className="text-center align-middle">15000</td>
                        <td className="text-center align-middle">
                            <Link to="/dashboard/product/edit/1" className="text-primary"><FontAwesomeIcon icon={faEdit}/></Link>
                            <a className="ms-2 text-danger"><FontAwesomeIcon icon={faTrash}/></a>
                        </td>
                    </tr>*/}

                    {
                        pagination.length !== 0 ?
                            pagination[page].map((d, i) => {
                                return (
                                    <tr key={i}>
                                        <th className="text-center align-middle" scope="row">{d.productName}</th>
                                        <td className="text-center align-middle">{d.category.categoryName}</td>
                                        <td className="text-center align-middle">{d.code.codeWord}</td>
                                        <td  className="product-picture"><img src={d.productImage != null ? "https://localhost:44326/img/" + d.productImage : ""} alt=""/></td>
                                        <td className="text-center align-middle">{d.qunatity}</td>
                                        <td className="text-center align-middle">{d.buyPrice}</td>
                                        <td className="text-center align-middle">{d.sellPrice}</td>
                                        {/*<td className="text-center align-middle">{parseInt(d.buyPrice) * parseInt(d.qunatity)}</td>*/}
                                        <td className="text-center align-middle">{d.totalSell}</td>
                                        <td className="text-center align-middle">{d.profit}</td>
                                        <td className="text-center align-middle">
                                            <Link to={"/dashboard/product/edit/" + d.productID} className="text-primary"><FontAwesomeIcon icon={faEdit}/></Link>
                                            <a onClick={event => deleteProduct(d.productID)} className="ms-2 text-danger"><FontAwesomeIcon icon={faTrash}/></a>
                                        </td>
                                    </tr>
                                )
                            }) : null
                    }
                    </tbody>
                </table>
            </div>
            <div className="d-flex justify-content-center">
                <Pagination
                    count={pagination.length - 1}
                    color="primary"
                    size={"small"}
                    shape="rounded"
                    onChange={pageChange}
                    page={page}
                />
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default Products;
