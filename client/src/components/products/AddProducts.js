import "./AddProducts.scss";
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import React from 'react';
import {useState, useEffect} from 'react';
import {useParams, useHistory} from 'react-router-dom';
import axios from "axios";
import Toast from "../toast/Toast";
import Keys from "../keys/Keys";

const AddProducts = () => {
    const history = useHistory();
    const [name, setName] = useState("");
    const [category, setCategory] = useState([]);
    const [categoryId, setCategoryId] = useState(0);
    const [code, setCode] = useState([]);
    const [codeId, setCodeId] = useState(0);
    const [qty, setQty] = useState("");
    const [buy, setBuy] = useState("");
    const [sell, setSell] = useState("");
    const [image, setImage] = useState("");
    const [imageName, setImageName] = useState("");

    const [url, setUrl] = useState("");
    const {id: eid} = useParams();

    useEffect(() => {
        axios.get(Keys.url + "/api/categories")
            .then(response => {
                if (response.data.length > 0) {
                    console.log(response.data);
                    setCategory(response.data);
                }
            });
        axios.get(Keys.url + "/api/codes")
            .then(response => {
                if (response.data.length > 0) {
                    console.log(response.data);
                    setCode(response.data);
                }
            });

       if (!eid) {
           setUrl(Keys.url + "/api/products/add");
       } else {
           setUrl(Keys.url + "/api/products/update");
           axios.get(Keys.url + "/api/products/get/"+eid)
               .then(response => {
                    setName(response.data.productName);
                    setCategoryId(response.data.category.categoryID);
                    setCodeId(response.data.code.codeID);
                    setQty(response.data.qunatity);
                    setSell(response.data.sellPrice);
                    setBuy(response.data.buyPrice);
                    setImageName(response.data.productImage);
               })
               .catch(err => {
                   alert("Something went wrong!")
               });
       }
    }, []);

    const addOrEditProduct = (event) => {
        event.preventDefault();

        // console.log("product added");
        if (categoryId !== 0 && name !== "" && codeId !== 0 && qty !== "" && buy !== "" && sell !== "") {
            // console.log(categoryId);

            let formData = new FormData();
            formData.append("ProductName", name);
            formData.append("Qunatity", qty.toString());
            formData.append("BuyPrice", buy);
            formData.append("SellPrice", sell);
            formData.append("CategoryID", categoryId);
            formData.append("CodeID", codeId);
            if (!eid) {
                if (image !== "") {
                    formData.append("imageFile", image);
                }
            } else {
                if (image !== "") {
                    formData.append("imageFile", image);
                    if (imageName !== "") {
                        formData.append("productImage", imageName);
                    }
                } else {
                    formData.append("productImage", imageName);
                }
                formData.append("productID", eid);
            }

            axios(url, {
                method: !eid ? 'POST' : 'PUT',
                headers: {
                    // "Content-Type": "multipart/form-data"
                },
                data: formData,
                // for laravel and node js
                // contentType: "application/json",
                // for .net
                processData: false,
                contentType: false,

            }).then(response => {
                Toast.successToast(response.data);
                history.push('/dashboard/products');
            }).catch((err) => {
                if (err.response.status === 400) {
                    Toast.warningToast(err.response.data);
                }
            });

        } else {
            Toast.dangerToast("The fields can not be empty!");
        }
    };


    return (
        <div className="add-products-container">
            <title>Dashboard - {!eid ? "Add" : "Edit"} products</title>
            <div className="row d-flex justify-content-center">
                <div className="col-lg-6 col-md-8">
                    <form onSubmit={addOrEditProduct}>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Product name</label>
                            <input value={name} onChange={event => setName(event.target.value)} type="text"
                                   placeholder="Product name" className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="inputState" className="form-label">Product category</label>
                            <select value={categoryId} onChange={event => setCategoryId(event.target.value)} id="inputState"
                                    className="form-control col-md-4 bg-dark">
                                <option value={0}>Choose...</option>
                                {
                                    category.length > 0 ?
                                        category.map((d, i) => {
                                            return (
                                                <option key={i} value={d.categoryID}>{d.categoryName}</option>
                                            )
                                        })
                                        : null
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="inputState" className="form-label">Product code for</label>
                            <select value={codeId} onChange={event => setCodeId(event.target.value)} id="inputState"
                                    className="form-control col-md-4 bg-dark">
                                <option value={0}>Choose...</option>
                                {
                                    code.length > 0 ?
                                        code.map((d, i) => {
                                            return (
                                                <option key={i} value={d.codeID}>{d.codeName}</option>
                                            )
                                        })
                                        : null
                                }
                            </select>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Product image</label>
                            <input onChange={event => setImage(event.target.files[0])} type="file" className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Product quantity</label>
                            <input value={qty} onChange={event => setQty(event.target.value)} type="number" placeholder="Quantity"
                                   className="form-control quantity-input bg-dark" min={1} max={200}/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Product buy price</label>
                            <input value={buy} onChange={event => setBuy(event.target.value)} type="text" placeholder="Buy price"
                                   className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Product sell price</label>
                            <input value={sell} onChange={event => setSell(event.target.value)} type="text" placeholder="Sell price"
                                   className="form-control bg-dark text-white-50"/>
                        </div>
                        <input type="submit" className={`btn form-control ${!eid ? "btn-outline-primary" : "btn-outline-success"}`}
                               value={!eid ? "Add" : "Update"}/>
                    </form>
                </div>
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default AddProducts;
