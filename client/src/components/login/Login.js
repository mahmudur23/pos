import "./Login.scss";
import React, {useState, useEffect} from 'react';
import {Link, useHistory} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import Toast from "../toast/Toast";
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import Keys from "../keys/Keys";

const Login = () => {
    const history = useHistory();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    useEffect(() => {
        let token = localStorage.getItem("Admin");
        if (token !== null) {
            axios.post(Keys.url + "/api/users/login",
                {

                },
                {
                    headers: {
                        Authorization: 'Bearer ' + token
                    }
                }
            ).then((response) => {
                history.push('/dashboard/home');
            }).catch((err) => {
                localStorage.removeItem("Admin");
            });
        }
    },[]);

    const login = (event) => {
        event.preventDefault();

        if (email !== "" && password !== "") {
            let loginData = {"Email": email, "Password": password};
            axios.post(Keys.url + "/api/users/authenticate",
                loginData
            ).then((response) => {
                console.log(response.data);
                localStorage.setItem("Admin", response.data);
                setTimeout(() => {
                    authenticate();
                }, 500);
            }).catch((err) => {
                console.log(err);
                if (err.response.status === 401) {
                    Toast.dangerToast(err.response.data);
                } else {
                    Toast.dangerToast("Your role does not have any permission to access!");
                }
            });
        }else {
            Toast.dangerToast("The fields can not be empty!");
        }
    };

    const authenticate = () => {
        let token = localStorage.getItem("Admin");
        if (token != null) {
            axios.post(Keys.url + "/api/users/login",
                {

                },
                {
                    headers: {
                        Authorization: 'Bearer ' + token
                    }
                }
            ).then((response) => {
                console.log(response.data);
                history.push('/dashboard/home');
            }).catch((err) => {
                console.log(err);
                Toast.dangerToast(err.response.data);
            });
        }
    };

    return (
        <div className="login-container">
            <title>User login</title>
            <div className="container">
                <div className="row d-flex justify-content-center">
                    <div className="col-lg-6 col-md-8">
                        <div className="card">
                            <div className="card-header bg-primary text-light">
                                Login
                            </div>
                            <div className="card-body">
                                <form onSubmit={login}>
                                    <div className="form-group mb-3">
                                        <label htmlFor="email" className="form-label">Email address</label>
                                        <input onChange={event => setEmail(event.target.value)} type="text" placeholder="Email" id="email" className="form-control"/>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label htmlFor="password" className="form-label">Password</label>
                                        <input onChange={event => setPassword(event.target.value)} type="password" placeholder="Password" id="password"
                                               className="form-control"/>
                                    </div>
                                    <div className="mb-3 form-check">
                                        <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                                            <label className="form-check-label" htmlFor="exampleCheck1">Remember</label>
                                    </div>
                                    <input type="submit" className="btn btn-outline-primary form-control"
                                           value="Login"/>
                                    <small className="text-muted">You don't have any account? <Link
                                        to="/register">Create an account</Link></small>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default Login;
