import "./Profile.scss";
import React, {useState, useEffect} from 'react';
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {ToastContainer} from "react-toastify";
import Toast from "../toast/Toast";
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import {login} from "../../actions";
import Keys from "../keys/Keys";

const Profile = () => {
    const history = useHistory();
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [inputPassword, setInputPassword] = useState("");
    const [phone, setPhone] = useState("");
    const [gender, setGender] = useState("Male");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [type, setType] = useState("");
    const [image, setImage] = useState("");
    const [previousImage, setPreviousImage] = useState("");
    const [id, setId] = useState(0);

    const dispatch = useDispatch();
    const auth = useSelector(state => state.Auth);

    useEffect(() => {
        if (auth !== 0) {
            setName(auth.name);
            setEmail(auth.email);
            setPhone(auth.phone? auth.phone:"");
            setGender(auth.gender?auth.gender:"");
            setAddress(auth.address?auth.address:"");
            setCity(auth.city?auth.city:"");
            setState(auth.state?auth.state:"");
            setType(auth.type?auth.type:"");
            setId(auth.userID);
            setPreviousImage(auth.userImage);
            setPassword(auth.password);
        }
    }, [auth]);

    const update = (event) => {
        event.preventDefault();
        let token = localStorage.getItem("Admin");

        if (name !== "" && email !== "" && phone !== "" && address !== "" && city !== "" && state !== "") {
            let formData = new FormData();
            formData.append("UserID", id);
            formData.append("Name", name);
            formData.append("Email", email);
            formData.append("Phone", phone);
            if (inputPassword.length > 10) {
                Toast.dangerToast("Password can not be more than ten characters!");
                return true;
            }
            if (inputPassword.length > 6) {
                formData.append("Password", inputPassword);
            } else {
                formData.append("Password", password);
            }
            if (image !== "") {
                formData.append("ImageFile", image);
            }
            if (previousImage) {
                formData.append("UserImage", previousImage);
            }
            formData.append("Gender", gender);
            formData.append("Address", address);
            formData.append("City", city);
            formData.append("State", state);
            formData.append("Type", type);

            axios(Keys.url + "/api/users/update", {
                method: 'PUT',
                headers: {
                    // "Content-Type": "multipart/form-data"
                    Authorization: 'Bearer ' + token
                    // 'Content-Type': 'multipart/form-data'
                },
                data: formData,
                // for laravel and node js
                // contentType: "application/json",
                // for .net
                processData: false,
                contentType: false,

            }).then(response => {
                reloadUser(token);
                Toast.successToast(response.data);
            }).catch((err) => {
                console.log(err);
                if (err.response.status === 400) {
                    Toast.warningToast(err.response.data);
                }
            });
        } else {
            Toast.dangerToast("The fields can not be empty!");
        }
    };

    const reloadUser = (token) => {
        if (token !== null) {
            axios.post(Keys.url + "/api/users/login",
                {

                },
                {
                    headers: {
                        Authorization: 'Bearer ' + token
                    }
                }
            ).then((response) => {
                dispatch(login(response.data));

            }).catch((err) => {
                console.log(err);
            });
        }
    };

    return (
        <div className={`profile-container`}>
            <title>Dashboard - Profile</title>
            <div className="row d-flex justify-content-center">
                <div className="col-lg-6 col-md-8">
                    <div className="d-flex justify-content-center">
                        <img className="mb-4 user-image" src={previousImage ? `${Keys.url}/user-img/` + previousImage : ""} alt="Customer DP"/>
                    </div>
                    <form onSubmit={update}>
                        <div className="form-group mb-3">
                            <label htmlFor="name" className="form-label">Name</label>
                            <input value={name} onChange={event => setName(event.target.value)}  type="text"
                                   placeholder="Name" className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Email</label>
                            <input value={email} onChange={event => setEmail(event.target.value)}  type="text"
                                    placeholder="Email" className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="phone" className="form-label">Phone</label>
                            <input value={phone} onChange={event => setPhone(event.target.value)}  type="text"
                                    placeholder="Phone" className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="image" className="form-label">Profile Image</label>
                            <input onChange={event => setImage(event.target.files[0])} type="file" className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="password" className="form-label">Password</label>
                            <input onChange={event => setInputPassword(event.target.value)}  type="password"
                                    placeholder="Password" className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="type" className="form-label">Type</label>
                            <input value={type} onChange={event => setType(event.target.value)}  type="text"
                                    placeholder="Type" className="form-control bg-dark text-white-50" disabled/>
                        </div>
                        <label>Gender</label>
                        <div className="form-check">
                            <input onChange={event => setGender(event.target.value)} checked={gender === "Male"} className="form-check-input bg-dark text-white-50" type="radio" name="exampleRadios"
                                   id="exampleRadios1" value="Male"/>
                            <label className="form-check-label" htmlFor="exampleRadios1">
                                Male
                            </label>
                        </div>
                        <div className="form-check">
                            <input onChange={event => setGender(event.target.value)} checked={gender === "Female"} className="form-check-input bg-dark text-white-50" type="radio" name="exampleRadios"
                                   id="exampleRadios2" value="Female"/>
                            <label className="form-check-label" htmlFor="exampleRadios2">
                                Female
                            </label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="address1" className="form-label">Address</label>
                            <input value={address} onChange={event => setAddress(event.target.value)} type="text" placeholder="Address" id="address"
                                   className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="city" className="form-label">City</label>
                            <input value={city} onChange={event => setCity(event.target.value)} type="text" placeholder="City" id="city" className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="mb-3 ">
                            <label htmlFor="inputState" className="form-label">State</label>
                            <select id="inputState" value={state} onChange={event => setState(event.target.value)} className="form-control col-md-4 bg-dark text-white-50">
                                <option value={""}>Choose...</option>
                                <option value={"Dhaka"}>Dhaka</option>
                                <option value={"Comilla"}>Comilla</option>
                                <option value={"Khulna"}>Khulna</option>
                                <option value={"Barishal"}>Barishal</option>
                                <option value={"Rajshahi"}>Rajshahi</option>
                            </select>
                        </div>
                        <input type="submit" className="btn btn-outline-success form-control"
                               value="Update"/>
                    </form>
                </div>
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default Profile;
