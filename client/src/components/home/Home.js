import "./Home.scss";
import React, {useEffect, useState} from 'react';
import axios from "axios";
import Keys from "../keys/Keys";

const Home = () => {
    const [products, setProducts] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [sells, setSells] = useState([]);
    const [profit, setProfit] = useState("");

    const showData = () => {
        axios.get(Keys.url + "/api/products")
            .then(res => {
                setProducts(res.data);
            })
            .catch(err => {
                alert("Something went wrong!")
            });

        axios.get(Keys.url + "/api/customers")
            .then(res => {
                setCustomers(res.data);
                // console.log(data);
            })
            .catch(err => {
                alert("Something went wrong!")
            });

        axios.get(Keys.url + "/api/sells")
            .then(res => {
                setSells(res.data);
            })
            .catch(err => {
                alert("Something went wrong!")
            });
    };

    useEffect(() => {
        showData();

    }, []);

    useEffect(() => {
        // Total profit
        let buy = 0;
        let sell = 0;
        let totalProfit = 0;
        sells.forEach(d => {
            d.productSells.forEach(p => {
                sell += (parseInt(p.product.sellPrice) * parseInt(p.quantity));
                buy += (parseInt(p.product.buyPrice) * parseInt(p.quantity));
            });
        });
        totalProfit = sell - buy;
        setProfit(totalProfit.toString());
    }, [customers, products, sells]);

    return (
        <div className="row card-container">
            <title>Dashboard - Home</title>
            <div className="col-lg-3 col-md-4 col-sm-6 col-12">
                <div className="card card-one bg-primary">
                    <div className="card-body">
                        <p className="card-title">{customers.length}</p>
                        <p className="card-subtitle">Total Customers</p>
                    </div>
                </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-12">
                <div className="card card-two bg-success">
                    <div className="card-body">
                        <p className="card-title">{products.length}</p>
                        <p className="card-subtitle">Total Products</p>
                    </div>
                </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-12">
                <div className="card card-three bg-warning">
                    <div className="card-body">
                        <p className="card-title">{sells.length}</p>
                        <p className="card-subtitle">Total Sells</p>
                    </div>
                </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-12">
                <div className="card card-four bg-danger">
                    <div className="card-body">
                        <p className="card-title">{profit} TK</p>
                        <p className="card-subtitle">Total Profit</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;
