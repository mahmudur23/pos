import "./Register.scss";
import React, {useEffect, useState} from 'react';
import {Link, useHistory} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import Toast from "../toast/Toast";
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import Keys from "../keys/Keys";

const Register = () => {
    const history = useHistory();
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [phone, setPhone] = useState("");
    const [gender, setGender] = useState("Male");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");


    useEffect(() => {
        let token = localStorage.getItem("Admin");
        if (token) {
            axios.post(Keys.url + "/api/users/login",
                {

                },
                {
                    headers: {
                        Authorization: 'Bearer ' + token
                    }
                }
            ).then((response) => {
                history.push('/dashboard/home');
            }).catch((err) => {
                localStorage.removeItem("Admin");
            });
        }
    },[]);

    const register = (event) => {
        event.preventDefault();

        if (name !== "" && email !== "" && phone !== "" && password !== "" && address !== "" && city !== "" && state !== "") {
            let userData = {"Name": name, "Email": email, "Password": password, "Phone": phone, "Gender": gender, "Address": address, "City": city, "State": state};
            axios.post(Keys.url + "/api/users/register",
                userData
            ).then((response) => {
                console.log(response.data);
                history.push('/login');
            }).catch((err) => {
                console.log(err);
                if (err.response.status === 400) {
                    Toast.dangerToast(err.response.data);
                }
            });
        } else {
            Toast.dangerToast("The fields can not be empty!");
        }
    };

    return (
        <div className="register-container">
            <title>User register</title>
            <div className="container">
                <div className="row d-flex justify-content-center">
                    <div className="col-lg-6 col-md-8">
                        <div className="card">
                            <div className="card-header bg-success text-light">
                                Register
                            </div>
                            <div className="card-body">
                                <form onSubmit={register}>
                                    <div className="mb-3">
                                        <label htmlFor="name" className="form-label">Name<span className="text-danger">*</span></label>
                                        <input onChange={event => setName(event.target.value)} type="text" placeholder="Full name" id="name"
                                               className="form-control"/>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">Email<span className="text-danger">*</span></label>
                                        <input onChange={event => setEmail(event.target.value)} type="email" placeholder="Email" id="email" className="form-control"/>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="phone" className="form-label">Phone<span className="text-danger">*</span></label>
                                        <input onChange={event => setPhone(event.target.value)} type="text" placeholder="Phone" id="phone" className="form-control"/>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">Password<span className="text-danger">*</span></label>
                                        <input onChange={event => setPassword(event.target.value)} type="password" placeholder="Password" id="password" className="form-control"/>
                                    </div>
                                    <label>Gender<span className="text-danger">*</span></label>
                                    <div className="form-check">
                                        <input onChange={event => setGender(event.target.value)} checked={gender === "Male"} className="form-check-input" type="radio" name="exampleRadios"
                                               id="exampleRadios1" value="Male"/>
                                        <label className="form-check-label" htmlFor="exampleRadios1">
                                            Male
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <input onChange={event => setGender(event.target.value)} checked={gender === "Female"} className="form-check-input" type="radio" name="exampleRadios"
                                               id="exampleRadios2" value="Female"/>
                                        <label className="form-check-label" htmlFor="exampleRadios2">
                                            Female
                                        </label>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="address1" className="form-label">Address<span className="text-danger">*</span></label>
                                        <input onChange={event => setAddress(event.target.value)} type="text" placeholder="Address" id="address"
                                               className="form-control"/>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="city" className="form-label">City<span className="text-danger">*</span></label>
                                        <input onChange={event => setCity(event.target.value)} type="text" placeholder="City" id="city" className="form-control"/>
                                    </div>
                                    <div className="mb-3 ">
                                        <label htmlFor="inputState" className="form-label">State<span className="text-danger">*</span></label>
                                        <select id="inputState" value={state} onChange={event => setState(event.target.value)} className="form-control col-md-4">
                                            <option value={""}>Choose...</option>
                                            <option value={"Dhaka"}>Dhaka</option>
                                            <option value={"Comilla"}>Comilla</option>
                                            <option value={"Khulna"}>Khulna</option>
                                            <option value={"Barishal"}>Barishal</option>
                                            <option value={"Rajshahi"}>Rajshahi</option>
                                        </select>
                                    </div>
                                    <input type="submit" className="btn btn-outline-success form-control"
                                           value="Register"/>
                                    <small className="text-muted">Already have an account? <Link
                                        to="/login">Login</Link></small>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default Register;
