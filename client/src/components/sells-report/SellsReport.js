import "./SellsReport.scss";
import React, {useEffect, useState} from 'react';
import Toast from "../toast/Toast";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faTrash, faEye} from "@fortawesome/free-solid-svg-icons";
import {Pagination} from "@material-ui/lab";
import axios from "axios";
import Keys from "../keys/Keys";
import {useDispatch, useSelector} from "react-redux";
import {pageAction} from "../../actions";
import {Link} from "react-router-dom";

const SellsReport = () => {

    const [data, setData] = useState([]);
    const paginationPerPage = 5;
    const [pagination, setPagination] = useState([]);
    const [page, setPage] = useState(1);
    const [search, setSearch] = useState();
    const [filter, setFilter] = useState([]);

    const showData = () => {
        axios.get(Keys.url + "/api/sells")
            .then(res => {
                setData(res.data);
                // console.log(data);
            })
            .catch(err => {
                alert("Something went wrong!")
            });
    };

    useEffect(() => {
        showData();
    }, []);

    const searchData = (event) => {
        setSearch(event.target.value);
    };

    // search
    useEffect(() => {
        setPage(1);
        let row = [];
        if (data.length !== 0) {
            data.filter((val) => {
                if (search === "" || search == null) {
                    row.push(val);
                    setFilter(row);
                } else if (val.sellID.toString().includes(search) || val.inserted.toLowerCase().includes(search.toLowerCase()) || val.customer.email.toLowerCase().includes(search.toLowerCase())) {
                    row.push(val);
                    setFilter(row);
                    // console.log(row);
                }
                return val;
            });
        }
    }, [search, data]);

    // pagination
    useEffect(() => {
        let temp = [];
        let tempArr = [];
        let paginationLength;
        let num = 0;
        if (filter.length != null) {
            paginationLength = Math.ceil(filter.length / paginationPerPage);
            // console.log(paginationLength);

            for (let i = 1; i <= paginationLength; i++) {
                for (let j = 0; j < paginationPerPage; j++) {
                    /*if (data[num] !== undefined) {
                        tempArr.push(data[num]);
                    }*/
                    if (filter[num] === undefined) {
                        break;
                    }

                    tempArr.push(filter[num]);
                    num++;
                    if (tempArr.length === paginationPerPage) {
                        break;
                    }
                }
                temp[i] = tempArr;
                tempArr = [];
            }
            setPagination(temp);
        }
        /*console.log(pagination);
        console.log(pagination.length);*/
    }, [filter]);

    const pageChange = (event, value) => {
        setPage(value);
        // console.log(value)
        dispatch(pageAction(value));
    };

    const deleteCustomer = (id) => {
        axios.delete(Keys.url + `/api/sells/delete/${id}`
        ).then((response) => {
            showData();
            Toast.successToast(response.data);
        }).catch((err) => {
            console.log(err)
        });
    };

    const dispatch = useDispatch();
    const pageValue = useSelector(state => state.Page);

    useEffect(() => {
        if ((pagination.length - 1) > 0 && pageValue !== 0) {
            if (pageValue <= pagination.length - 1) {
                setPage(pageValue);
            } else if (pageValue > pagination.length - 1) {
                setPage(pagination.length - 1);
                dispatch(pageAction(pagination.length - 1));
            }
            // console.log("Page value: " + pageValue);
        }
    }, [pagination]);

    return (
        <div className={`sells-list-container`}>
            <title>Dashboard - Sells List</title>
            <div className="row mb-1">
                <div className="col-md-4 col-sm-5 col-12 ms-auto">
                    <div className="form-group">
                        <input type="text" onChange={event => searchData(event)}
                               placeholder="Search" name="search"
                               className="form-control bg-dark text-white-50 shadow-none"/>
                    </div>
                </div>
            </div>
            <div className="table-container">
                <table className="table table-bordered table-dark">
                    <thead>
                    <tr className="">
                        <th scope="col" className="text-center">Sell ID</th>
                        <th scope="col" className="text-center">Customer Name</th>
                        <th scope="col" className="text-center">Phone</th>
                        <th scope="col" className="text-center">Email</th>
                        <th scope="col" className="text-center">Picture</th>
                        <th scope="col" className="text-center">Payment</th>
                        <th scope="col" className="text-center">Date</th>
                        <th scope="col" className="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {/*<tr>
                        <th className="text-center align-middle" scope="row">Akash Ahmed</th>
                        <td className="text-center align-middle">64734646734</td>
                        <td className="text-center align-middle">akashahmed@gmail.com</td>
                        <td  className="customer-picture"><img src={imageFive} alt=""/></td>
                        <td className="text-center align-middle">5000</td>
                        <td className="text-center align-middle">5000</td>
                        <td className="text-center align-middle">5000</td>
                        <td className="text-center align-middle">0</td>
                        <td className="text-center align-middle">
                            <Link to="/dashboard/customer/details/1" className="text-primary"><FontAwesomeIcon icon={faEye}/></Link>
                            <Link to="/dashboard/customer/edit/1" className="ms-2 text-warning"><FontAwesomeIcon icon={faEdit}/></Link>
                            <a className="ms-2 text-danger"><FontAwesomeIcon icon={faTrash}/></a>
                        </td>
                    </tr>*/}
                    {
                        pagination.length !== 0 ?
                            pagination[page].map((d, i) => {
                                return (
                                    <tr key={i}>
                                        <th className="text-center align-middle" scope="row">{d.sellID}</th>
                                        <td className="text-center align-middle">{d.customer.customerName}</td>
                                        <td className="text-center align-middle">{d.customer.phone}</td>
                                        <td className="text-center align-middle">{d.customer.email}</td>
                                        <td  className="customer-picture"><img src={d.customer.customerImage != null ? "https://localhost:44326/customer-img/" + d.customer.customerImage : ""} alt=""/></td>
                                        <td className="text-center align-middle">{d.payment}</td>
                                        <td className="text-center align-middle">{d.inserted.split("T")[0]}</td>
                                        <td className="text-center align-middle">
                                            <Link to={"/dashboard/product/sell-details/" + d.sellID} className="text-primary"><FontAwesomeIcon icon={faEye}/></Link>
                                            <a onClick={event => deleteCustomer(d.sellID)} className="ms-2 text-danger"><FontAwesomeIcon icon={faTrash}/></a>
                                        </td>
                                    </tr>
                                )
                            }) : null
                    }
                    </tbody>
                </table>
            </div>
            <div className="d-flex justify-content-center">
                <Pagination
                    count={pagination.length - 1}
                    color="primary"
                    size={"small"}
                    shape="rounded"
                    onChange={pageChange}
                    page={page}
                />
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default SellsReport;
