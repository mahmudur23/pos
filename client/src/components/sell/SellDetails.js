import './SellDetails.scss';
import React, {useEffect, useRef, useState} from 'react';
import {useParams} from 'react-router-dom';
import axios from "axios";
import Invoice from "./Invoice";
import Keys from "../keys/Keys";
import {useReactToPrint} from "react-to-print";

const SellDetails = () => {
    const {id: eid} = useParams();
    const [sellData, setSellData] = useState("");

    useEffect(() => {
        if (eid) {
            axios.get(Keys.url + "/api/sells/get/"+eid)
                .then(response => {
                    setSellData(response.data);
                })
                .catch(err => {
                    alert("Something went wrong!")
                });
        }
    }, []);

    const componentRef = useRef();
    const printInvoice = useReactToPrint({
        content: () => componentRef.current,
        documentTitle: "invoice.pdf",
    });

    const print = () => {
        printInvoice();
        console.log("Printed");
    };

    return (
        <div className='sell-details-container'>
            <title>Dashboard - Sell Details</title>
            {
                sellData !== "" ?
                    <div>
                        <div className="row invoice-container d-flex justify-content-center">
                            <div className="col-md-8 bg-light p-4">
                                <Invoice ref={componentRef} sellData = {sellData !== "" ? sellData : null} className="d-none"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 text-center">
                                <button onClick={event => print()} className="btn btn-success print-button mb-3">Print</button>
                            </div>
                        </div>
                    </div> : <div></div>
            }
        </div>
    );
};

export default SellDetails;
