import "./Sell.scss";
import React, {useState, useEffect, useRef} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faTrash} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import Toast from "../toast/Toast";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Invoice from "./Invoice";
import {useReactToPrint} from "react-to-print";
import Keys from "../keys/Keys";

const Sell = () => {
    const [qty, setQty] = useState(1);
    const [data, setData] = useState([]);
    const [customer, setCustomer] = useState([]);
    const [products, setProducts] = useState([]);
    const [sellProducts, setSellProducts] = useState([]);
    const [code, setCode] = useState("");
    const [totalPrice, setTotalPrice] = useState(0);

    const [customerName, setCustomerName] = useState("");
    const [customerId, setCustomerId] = useState(0);
    const [image, setImage] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");

    const [sellData, setSellData] = useState("");


    const showProducts = () => {
        axios.get(Keys.url + "/api/products")
            .then(response => {
                setData(response.data);
                // console.log(data);
                // console.log(response.status);
            })
            .catch(err => {
                alert("Something went wrong!")
            });
    };

    const showCustomers = () => {
        axios.get(Keys.url + "/api/customers")
            .then(response => {
                setCustomer(response.data);
                // console.log(data);
                // console.log(response.status);
            })
            .catch(err => {
                alert("Something went wrong!")
            });
    };

    useEffect(() => {
        showProducts();
        showCustomers();
    }, []);


    useEffect(() => {
        if (email.length > 0) {
            for (let i = 0; i < customer.length; i++) {
                if (customer[i].email === email) {
                    setCustomerName(customer[i].customerName);
                    setPhone(customer[i].phone);
                    setCustomerId(customer[i].customerID);
                    setImage(customer[i].customerImage);
                    break;
                }

            }
        } else {
            setCustomerName("");
            setPhone("");
            setCustomerId(0);
            setImage("");
        }
    }, [email]);

    const findProduct = (id) => {
        if (products.length > 0) {
            for (let i = 0; i < products.length; i++) {
                if (products[i].productID === id) {
                    return true;
                }
            }
            return false;
        }
    };

    useEffect(() => {
        let temp = [];
        let action = false;
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].code.codeWord === code) {
                    if (findProduct(data[i].productID) === true) {
                        Toast.warningToast("Product is already inserted!");
                        break;
                    }
                    if (parseInt(data[i].qunatity) < 1) {
                        Toast.dangerToast("Product is out of stock!");
                        break;
                    }
                    action = true;
                    temp.push(...products, {
                        "productID": data[i].productID,
                        "productName": data[i].productName,
                        "sellPrice": data[i].sellPrice,
                        "qty": qty,
                        "categoryID": data[i].categoryID,
                        "codeID": data[i].codeID,
                        "productImage": data[i].productImage,
                        "qunatity": data[i].qunatity,
                        "buyPrice": data[i].buyPrice
                    });
                    break;
                }
            }
        }

        if (action === true) {
            setProducts(temp);
            temp = [];
            action = false;
        }
    }, [code]);

    useEffect(() => {
        setCode("");
        // console.log(products);
        // console.log(products.length);

        let total = 0;
        if (products.length > 0) {
            products.forEach(d => {
               total += d.sellPrice * d.qty
            });
        }
        setTotalPrice(total);
    }, [products]);

    const updateProduct = (qty, id) => {
        setQty(qty);
        let temp = [];
        for (let i = 0; i < products.length; i++) {
            if (products[i].productID === id) {
                if (parseInt(products[i].qunatity) - 1 < qty) {
                    setQty(qty - 1);
                    Toast.dangerToast("Out of stock!");
                    break;
                }
                products[i].qty = qty;
                break;
            }
        }
        temp.push(...products);
        setProducts(temp);
        setQty(1);
    };

    const deleteProduct = (id) => {
        let temp = [];
        for (let i = 0; i < products.length; i++) {
            if (products[i].productID === id) {
                products[i] = undefined;
            }
        }
        products.forEach(p => {
            if (p !== undefined) {
                temp.push(p);
            }
        });
        setProducts(temp);
    };

    const sellProduct = () => {
        if (customerId !== 0 && products.length > 0) {

            products.forEach(d => {
                if (d.qty > parseInt(d.qunatity)) {
                    Toast.dangerToast("Product is out of stock!");
                    return true;
                }
                sellProducts.push({
                    "productID": d.productID,
                    "quantity": d.qty
                });
            });
            setSellProducts(sellProducts);
            updateProductModel();

            let sellsData = {"customerID": customerId, "payment": totalPrice, "productSells": sellProducts};

            axios.post(Keys.url + "/api/sells/add",
                sellsData
            ).then((response) => {
                // console.log(response.data);
                setSellData(response.data);
                Toast.successToast("Sells saved successfully!");
                setProducts([]);
                setSellProducts([]);
                setEmail("");
            }).catch((err) => {
                console.log(err)
            });
        } else {
            Toast.warningToast("Customer and product should not be empty!");
        }
    };

    const updateProductModel = () => {
        let temp = [];

        products.forEach(d => {
            temp.push({
                "productID": d.productID,
                "productName": d.productName,
                "sellPrice": d.sellPrice,
                "categoryID": d.categoryID,
                "codeID": d.codeID,
                "productImage": d.productImage,
                "qunatity": parseInt(d.qunatity) - d.qty,
                "buyPrice": d.buyPrice
            });
        });
        console.log("Product model: ");
        console.log(temp);
        axios.put(Keys.url + "/api/products/update-sell",
            temp
        ).then((response) => {
            console.log(response.data);
            showProducts();
        }).catch((err) => {
            console.log(err)
        });
    };

    const componentRef = useRef();
    const printInvoice = useReactToPrint({
        content: () => componentRef.current,
        documentTitle: "invoice.pdf",
    });

    const print = () => {
        printInvoice();
        console.log("Printed");
        setSellData("");
    };

    return (
        <div className="sell-container">
            <title>Dashboard - Product Sell</title>
            <div className="row">
                <div className="col-lg-4 col-md-5 col-md-7 col-sm-10 col-12 mx-auto">
                    <div className="form-group mb-3">
                        <label htmlFor="email" className="form-label">Enter customer name or email</label>
                        <input value={email} onChange={event => setEmail(event.target.value)}
                               type="text" placeholder="Customer"  className="form-control bg-dark text-white-50"/>
                    </div>
                    <img className="mb-4 customer-image" src={image !== "" ? "https://localhost:44326/customer-img/" + image : ""} alt="Customer DP"/>
                    <div className="details-item mb-4">
                        <span className="details-heading">Customer name: </span> {customerName}
                    </div>
                    <div className="details-item mb-4">
                        <span className="details-heading">Customer phone: </span> {phone}
                    </div>
                </div>
                <div className="col-lg-4 col-md-5 col-md-7 col-sm-10 col-12 mx-auto">
                    <div className="form-group mb-3">
                        <label htmlFor="email" className="form-label">Enter product code</label>
                        <input value={code} onChange={event => setCode(event.target.value)}
                            type="text" placeholder="Product code"  className="form-control bg-dark text-white-50"/>
                    </div>
                    <table className="table table-bordered table-dark">
                        <thead>
                        <tr>
                            <th scope="col" className="text-center">Product Name</th>
                            <th scope="col" className="text-center">Price</th>
                            <th scope="col" className="text-center">Qty</th>
                            <th scope="col" className="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            products.length !== 0 ?
                                products.map((d, i) => {
                                    return (
                                        <tr key={i}>
                                            <th className="text-center align-middle" scope="row">{d.productName}</th>
                                            <td className="text-center align-middle">{d.sellPrice} TK</td>
                                            <td className="text-center d-flex justify-content-center">
                                                <input value={d.qty} onChange={event => updateProduct(event.target.value, d.productID)} type="number" placeholder="Quantity"
                                                       className="form-control quantity-input bg-dark text-white-50 quantity-input-field" min={1} max={12}/>
                                            </td>
                                            <td className="text-center align-middle">
                                                <a onClick={event => deleteProduct(d.productID)} className="ms-2 text-danger"><FontAwesomeIcon icon={faTrash}/></a>
                                            </td>
                                        </tr>
                                    )
                                }) : null
                        }
                        </tbody>
                    </table>
                    <div className="form-group mb-3">
                        <h5 className="ms-auto">Total Price: {totalPrice} TK</h5>
                    </div>
                    <button onClick={sellProduct} className="btn btn-outline-success">Sell Product</button>
                </div>
            </div>
            {
                sellData !== "" ?
                    <div>
                        <div className="row invoice-container d-flex justify-content-center">
                            <div className="col-md-8 bg-light p-4">
                                <Invoice ref={componentRef} sellData = {sellData !== "" ? sellData : null} className="d-none"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 text-center">
                                <button onClick={event => print()} className="btn btn-success print-button mb-3">Print</button>
                            </div>
                        </div>
                    </div> : <div></div>
            }
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default Sell;
