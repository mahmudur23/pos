import "./Invoice.scss";
import React, {Component} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash} from "@fortawesome/free-solid-svg-icons";


class Invoice extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: this.props.sellData
        };
    }

    componentDidMount() {
        console.log(this.data);
    }

    render() {
        return (
            <div className={`invoice-container bg-dark container`}>
                <div className="bg-danger label-box"></div>
                <h1 className={`invoice-title`}>INVOICE</h1>
                <div className="fist-row row">
                    <div className="col-md-4">
                        <p>Inventory Shop</p>
                        <p>Dhaka, Bangladesh</p>
                        <p>1800</p>
                        <p>748748374</p>
                        <p>akash@gmail.com</p>
                    </div>
                    <div className="col-md-4 text-center ms-auto">
                        <p>DATE: {this.props.sellData !== "" ? this.props.sellData.inserted.split("T")[0]: null}</p>
                        <p>INVOICE ID: {this.props.sellData !== "" ? this.props.sellData.sellID: null}</p>
                    </div>
                </div>
                <div className="second-row row">
                    <div className="col-md-4 ">
                        <h6>BILL TO</h6>
                        <hr/>
                        <p>{this.props.sellData !== "" ? this.props.sellData.customer.customerName: null}</p>
                        <p>{this.props.sellData !== "" ? this.props.sellData.customer.email: null}</p>
                        <p>{this.props.sellData !== "" ? this.props.sellData.customer.phone: null}</p>
                    </div>
                </div>
                <div className="row">
                    <table className="table table-bordered table-dark">
                        <thead>
                        <tr>
                            <th scope="col" className="text-center">Product Name</th>
                            <th scope="col" className="text-center">Qty</th>
                            <th scope="col" className="text-center">Price</th>
                            <th scope="col" className="text-center">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.sellData !== "" ?
                                this.props.sellData.productSells.map((d, i) => {
                                    return (
                                        <tr key={i}>
                                            <th className="text-center align-middle" scope="row">{d.product.productName}</th>
                                            <td className="text-center align-middle">{d.quantity}</td>
                                            <td className="text-center align-middle">{d.product.sellPrice} TK</td>
                                            <td className="text-center align-middle">{(parseInt(d.product.sellPrice) * parseInt(d.quantity)).toString()} TK</td>
                                        </tr>
                                    )
                                }) :
                                <div></div>
                        }
                        </tbody>
                    </table>
                    <div className="row">
                        <h5 className="text-end">Total Price: {this.props.sellData !== "" ? this.props.sellData.payment: null} TK</h5>
                    </div>
                </div>
            </div>
        );
    }
}

export default Invoice;
