import "./CustomerDetails.scss";
import React from 'react';

import imageOne from "../../images/1.jpg";

const CustomerDetails = () => {
    return (
        <div className="customer-details-container">
            <title>Dashboard - Customer details</title>
            <div className="row">
                <div className="col-xl-4 col-md-5 col-md-6 col-sm-10 col-12 m-auto">
                    <img className="mb-4" src={imageOne} alt="Customer DP"/>
                    <div className="details-item mb-4">
                        <span className="details-heading">Customer name: </span> Akash Ahmed
                    </div>
                    <div className="details-item mb-4">
                        <span className="details-heading">Customer phone: </span> 4673434434
                    </div>
                    <div className="details-item mb-4">
                        <span className="details-heading">Customer email: </span> akashahmed@gmail.com
                    </div>
                    <div className="details-item mb-4">
                        <span className="details-heading">Customer phone: </span> 4673434434
                    </div>
                    <div className="details-item mb-4">
                        <span className="details-heading">Previous due: </span> 5000
                    </div>
                    <div className="details-item mb-4">
                        <span className="details-heading">Total: </span> 5000
                    </div>
                    <div className="details-item mb-4">
                        <span className="details-heading">Paid: </span> 5000
                    </div>
                    <div className="details-item mb-4">
                        <span className="details-heading">Balance: </span> 0
                    </div>
                    <button className="btn btn-outline-success">Download Report</button>
                </div>
            </div>
        </div>
    );
};

export default CustomerDetails;
