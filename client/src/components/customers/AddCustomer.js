import "./AddCustomer.scss";
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Toast from "../toast/Toast";
import React from 'react';
import {useState, useEffect} from 'react';
import {useParams, useHistory} from 'react-router-dom';
import axios from "axios";
import Keys from "../keys/Keys";

const AddCustomer = () => {
    const history = useHistory();
    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [image, setImage] = useState("");
    const [imageName, setImageName] = useState("");
    const [due, setDue] = useState(0);

    const [url, setUrl] = useState("");
    const {id: eid} = useParams();

    useEffect(() => {
        if (!eid) {
            setUrl(Keys.url + "/api/customers/add");
        } else {
            setUrl(Keys.url + "/api/customers/update");
            axios.get(Keys.url + "/api/customers/get/"+eid)
                .then(response => {
                    setName(response.data.customerName);
                    setPhone(response.data.phone);
                    setEmail(response.data.email);
                    setImageName(response.data.customerImage);
                    setDue(response.data.previousDue);
                })
                .catch(err => {
                    alert("Something went wrong!")
                });
        }
    }, []);

    const addOrEditCustomer = (event) => {
        event.preventDefault();

        if (name !== "" && phone !== "" && email !== "") {
            let formData = new FormData();
            formData.append("CustomerName", name);
            formData.append("Phone", phone);
            formData.append("Email", email);
            formData.append("PreviousDue", due);
            if (!eid) {
                if (image !== "") {
                    formData.append("imageFile", image);
                }
            } else {
                if (image !== "") {
                    formData.append("imageFile", image);
                    if (imageName !== "") {
                        formData.append("CustomerImage", imageName);
                    }
                } else {
                    formData.append("CustomerImage", imageName);
                }
                formData.append("CustomerID", eid);
            }

            axios(url, {
                method: !eid ? 'POST' : 'PUT',
                headers: {
                    // "Content-Type": "multipart/form-data"
                },
                data: formData,
                // for laravel and node js
                // contentType: "application/json",
                // for .net
                processData: false,
                contentType: false,

            }).then(response => {
                Toast.successToast(response.data);
                resetForm();
                // history.push('/dashboard/customers');
            }).catch((err) => {
                if (err.response.status === 400) {
                    Toast.dangerToast(err.response.data);
                }
            });
        } else {
            Toast.dangerToast("The fields can not be empty!");
        }
    };

    const resetForm = () => {
        let form = document.getElementById("customer-form-id");
        form.reset();
    };

    return (
        <div className="add-customer-container">
            <title>Dashboard - {!eid ? "Add" : "Edit"} Customers</title>
            <div className="row d-flex justify-content-center">
                <div className="col-lg-6 col-md-8">
                    <form id="customer-form-id" onSubmit={addOrEditCustomer}>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Customer name</label>
                            <input value={name} onChange={event => setName(event.target.value)}
                                type="text" placeholder="Customer name"  className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Phone</label>
                            <input value={phone} onChange={event => setPhone(event.target.value)}
                                type="text" placeholder="Phone"  className="form-control quantity-input bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Email</label>
                            <input value={email} onChange={event => setEmail(event.target.value)}
                                type="text" placeholder="Email"  className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Customer image</label>
                            <input onChange={event => setImage(event.target.files[0])} type="file"  className="form-control bg-dark text-white-50"/>
                        </div>
                        {/*<div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Previous due</label>
                            <input value={due} onChange={event => setDue(event.target.value)}
                                type="text" placeholder="Previous due"  className="form-control quantity-input bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Paid</label>
                            <input type="text" placeholder="Paid"  className="form-control bg-dark text-white-50"/>
                        </div>*/}
                        <input type="submit" className={`btn form-control ${!eid ? "btn-outline-primary" : "btn-outline-success"}`}
                               value={!eid ? "Add" : "Update"}/>
                    </form>
                </div>
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default AddCustomer;
