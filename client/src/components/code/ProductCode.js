import "./ProductCode.scss";
import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faEdit,
    faTrash,
    faEye
} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import Toast from "../toast/Toast";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import Keys from "../keys/Keys";

const ProductCode = () => {
    const [code, setCode] = useState("");
    const [codeName, setCodeName] = useState("");
    const [edit, setEdit] = useState(false);
    const [url, setUrl] = useState("");
    const [id, setId] = useState(0);
    const [data, setData] = useState([]);

    const showData = () => {
        axios.get(Keys.url + "/api/codes")
            .then(res => {
                setData(res.data);
                // console.log(data);
                // console.log(res.status);
            })
            .catch(err => {
                alert("Something went wrong!")
            });
    };

    useEffect(() => {
        showData();
    }, []);

    const codeAddOrEdit = (event) => {
        event.preventDefault();

        if (code !== "" && codeName !== "") {
            const codeData = {"CodeName": codeName, "CodeWord": code};

            if (!edit) {
                setUrl(Keys.url + "/api/codes/add");
                axios.post(url,
                    codeData
                ).then((response) => {
                    console.log(response.data);
                    Toast.successToast(response.data);
                    showData();
                    resetForm();
                }).catch((err) => {
                    if (err.response.status === 400) {
                        Toast.dangerToast(err.response.data);
                    }
                });
            } else {
                setUrl(Keys.url + "/api/codes/update");
                codeData["codeID"] = id;
                axios.put(url,
                    codeData
                ).then((response) => {
                    console.log(response.data);
                    Toast.successToast(response.data);
                    showData();
                    cancelHandler(false);
                }).catch((err) => {
                    console.log(err)
                });
            }

        } else {
            Toast.dangerToast("The fields can not be empty!");
        }
    };

    const editHandler = (value, id) => {
        setEdit(value);
        setId(id);

        axios.get(Keys.url + "/api/codes/get/" + id)
            .then(res => {
                setCode(res.data.codeWord);
                setCodeName(res.data.codeName);
                // console.log(res.data);
            })
            .catch(err => {
                alert("Something went wrong!")
            });
    };

    const deleteCode = (id) => {
        axios.delete(Keys.url + `/api/codes/delete/${id}`
        ).then((response) => {
            showData();
            Toast.successToast(response.data);
        }).catch((err) => {
            if (err.response.status === 400) {
                Toast.warningToast(err.response.data);
            }
        })
    };

    const cancelHandler = (value) => {
        setEdit(value);
        resetForm();
    };

    const resetForm = () => {
        setCode("");
        setCodeName("");
    };

    return (
        <div className="products-code-container">
            <title>Dashboard - Products code</title>
            <div className="row">
                <div className="col-lg-4 col-md-5 col-md-7 col-sm-10 col-12 mx-auto">
                    <table className="table table-bordered table-dark">
                        <thead>
                        <tr>
                            <th scope="col" className="text-center">Product code</th>
                            <th scope="col" className="text-center">Code name</th>
                            <th scope="col" className="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            data.length !== 0 ?
                                data.map((d, i) => {
                                    return (
                                        <tr key={i}>
                                            <th className="text-center align-middle" scope="row">{d.codeWord}</th>
                                            <td className="text-center align-middle">{d.codeName}</td>
                                            <td className="text-center align-middle">
                                                <a onClick={event => editHandler(true, d.codeID)} className="ms-2 text-warning"><FontAwesomeIcon icon={faEdit}/></a>
                                                <a onClick={event => deleteCode(d.codeID)} className="ms-2 text-danger"><FontAwesomeIcon icon={faTrash}/></a>
                                            </td>
                                        </tr>
                                    )
                                }) : null
                        }
                        </tbody>
                    </table>
                </div>
                <div className="col-lg-4 col-md-5 col-md-7 col-sm-10 col-12 mx-auto">
                    <form onSubmit={codeAddOrEdit}>
                        <div className="form-group mb-3">
                            <label htmlFor="email" className="form-label">Product code</label>
                            <input value={code} onChange={event => setCode(event.target.value)}
                                type="text" placeholder="Product code" id="product-code-id" className="form-control bg-dark text-white-50"/>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="password" className="form-label">Product name</label>
                            <input value={codeName} onChange={event => setCodeName(event.target.value)}
                                type="text" placeholder="Product name" id="product-code-name-id"
                                   className="form-control bg-dark text-white-50"/>
                        </div>
                        <input type="submit" className={`btn form-control ${!edit ? "btn-outline-primary" : "btn-outline-success"}`}
                               value={!edit ? "Add" : "Update"}/>
                        {
                            edit ? <input type={"button"} onClick={event => cancelHandler(false)} className="btn btn-outline-danger mt-2 form-control"
                                          value="Cancel"/>
                                : null
                        }
                    </form>
                </div>
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

export default ProductCode;
