import "bootstrap";
import './App.scss';
import React from "react";
import {Suspense, lazy} from "react";
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import Dashboard from "./admin/Dashboard";

const loading = (
    <div className="bg-dark text-light d-flex justify-content-center align-items-center" style={{width: "100%", height: "100vh"}}>
        <div className="spinner-border" style={{width: "3rem", height: "3rem"}} role="status">
            <span className="visually-hidden">Loading...</span>
        </div>
        <div className="spinner-grow text-light" style={{width: "3rem", height: "3rem"}} role="status">
            <span className="visually-hidden">Loading...</span>
        </div>
    </div>
);

// const Dashboard = lazy(() => import("./admin/Dashboard"));
const Login = lazy(() => import("./components/login/Login"));
const Register = lazy(() => import("./components/register/Register"));

function App() {
    return (
        <div>
            <Router>
                <Suspense fallback={loading}>
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/dashboard/home"/>
                        </Route>
                        <Route exact path="/dashboard">
                            <Redirect to="/dashboard/home"/>
                            <Dashboard/>
                        </Route>
                        <Route path="/dashboard/home" component={Dashboard}/>
                        <Route path="/dashboard/products" component={Dashboard}/>
                        <Route path="/dashboard/product/code" component={Dashboard}/>
                        <Route path="/dashboard/product/category" component={Dashboard}/>
                        <Route path="/dashboard/product/add" component={Dashboard}/>
                        <Route path="/dashboard/product/edit/:id" component={Dashboard}/>
                        <Route path="/dashboard/product/sell" component={Dashboard}/>
                        <Route path="/dashboard/product/sells" component={Dashboard}/>
                        <Route path="/dashboard/product/sell-details/:id" component={Dashboard}/>
                        <Route path="/dashboard/customers" component={Dashboard}/>
                        <Route path="/dashboard/profile" component={Dashboard}/>
                        <Route path="/dashboard/customer/:id" component={Dashboard}/>
                        <Route path="/dashboard/customer/add" component={Dashboard}/>
                        <Route path="/dashboard/customer/edit/:id" component={Dashboard}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/register" component={Register}/>
                        <Route path="*">
                            <h2>Not Found</h2>
                        </Route>
                    </Switch>
                </Suspense>
            </Router>
        </div>
    );
}

export default App;
