// Reducers index
import {combineReducers} from 'redux'
import Auth from './Auth'
import Page from './Page';

const allReducers = combineReducers({
    Auth,
    Page
});

export default allReducers;
