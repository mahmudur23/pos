const auth = (state = 0, {type, payload}) => {
    switch (type) {
        case 'LOGIN':
            if (payload !== null) {
                return payload;
            }
            return state;
        default:
            return state;
    }
};

export default auth;
