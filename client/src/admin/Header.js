import React, {useState, useEffect} from 'react';
import {Link, useHistory} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import Keys from "../components/keys/Keys";
import {useDispatch, useSelector} from "react-redux";
import {login} from "../actions";

const Header = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const auth = useSelector(state => state.Auth);

    useEffect(() => {
        let token = localStorage.getItem("Admin");
        if (token !== null) {
            axios.post(Keys.url + "/api/users/login",
                {

                },
                {
                    headers: {
                        Authorization: 'Bearer ' + token
                    }
                }
            ).then((response) => {
                // console.log(response.data);
                dispatch(login(response.data));

            }).catch((err) => {
                localStorage.removeItem("Admin");
                history.push('/login');
            });
        } else {
            history.push('/login');
        }
    }, []);

    const logout = () => {
        let token = localStorage.getItem("Admin");
        if (token !== null) {
            axios.post(Keys.url + "/api/users/logout",
                {

                },
                {
                    headers: {
                        Authorization: 'Bearer ' + token
                    }
                }
            ).then((response) => {
                console.log(response.data);
                localStorage.removeItem("Admin");
                history.push('/login');
            }).catch((err) => {
                history.push('/login');
            });
        }
    };

    return (
        <nav className="sb-topnav navbar navbar-expand card-header navbar-dark bg-dark" style={{borderRadius: 0}}>
            {/* Navbar Brand*/}
            <Link className="navbar-brand ps-3 text-lg-center" to="/dashboard">Admin</Link>
            {/* Sidebar Toggle*/}
            {/*<button className="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle"*/}
            <button className="btn btn-link btn-sm order-1 order-lg-0 me-lg-0" id="sidebarToggle"
                    href="#!"><FontAwesomeIcon icon={faBars}/></button>
            {/*<ul className="text-light navbar-nav ms-auto me-lg-4">*/}
            <ul className="text-light navbar-nav ms-auto">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        <img className="dropdown-logo" src={auth ? `${Keys.url}/user-img/` + auth.userImage : null} alt="Profile Picture"/></a>
                    <ul className="dropdown-menu dropdown-menu-end dropdown-menu-dark" aria-labelledby="navbarDropdown">
                        <li><Link className="dropdown-item" to="/dashboard/profile">Profile</Link></li>
                        <li><a className="dropdown-item" href="#!">Activity Log</a></li>
                        <li>
                            <hr className="dropdown-divider"/>
                        </li>
                        <li><a onClick={event => logout()} className="dropdown-item" href="#!">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    );
};

export default Header;
