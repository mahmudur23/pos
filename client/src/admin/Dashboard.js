import "./css/sb-admin.css";
import "./Dashboard.scss";
import React, {lazy} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import "./js/scripts";
import {} from '@fortawesome/free-brands-svg-icons';
import {} from "@fortawesome/free-solid-svg-icons";
import Footer from "./Footer";
import Header from "./Header";
import Sidebar from "./Sidebar";


// lazy(() => import());

const AddProducts = lazy(() => import("../components/products/AddProducts"));
const ProductCode = lazy(() => import("../components/code/ProductCode"));
const Category = lazy(() => import("../components/products/Category"));
const SellsReport =  lazy(() => import("../components/sells-report/SellsReport"));
const Customers = lazy(() => import("../components/customers/Customers"));
const CustomerDetails = lazy(() => import("../components/customers/CustomerDetails"));
const AddCustomer =  lazy(() => import("../components/customers/AddCustomer"));
const Home = lazy(() => import("../components/home/Home"));
const Products = lazy(() => import("../components/products/Products"));
const Sell = lazy(() => import("../components/sell/Sell"));
const Profile = lazy(() => import("../components/profile/Profile"));
const SellDetails = lazy(() => import("../components/sell/SellDetails"));

// images

const Dashboard = () => {
    return (
        <div className="sb-nav-fixed">
            <div>
                <Header/>
                <div id="layoutSidenav">
                    <Sidebar/>
                    {/*<div id="layoutSidenav_content" className="" style={{backgroundColor: "#545663", color: "#a3a7b8"}}>*/}
                    <div id="layoutSidenav_content" className="bg-dark" style={{color: "#a3a7b8"}}>
                        <main className="main-container">
                            <div className="container-fluid px-4">
                                {/*<h1 className="mt-4">Dashboard</h1>
                                <ol className="breadcrumb mb-4">
                                    <li className="breadcrumb-item active">Dashboard</li>
                                </ol>*/}
                                <Switch>
                                    <Route path="/dashboard/home" component={Home}/>
                                    <Route path="/dashboard/products" render={props => <Products {...props} />}/>
                                    <Route path="/dashboard/product/code" component={ProductCode}/>
                                    <Route path="/dashboard/product/category" component={Category}/>
                                    <Route path="/dashboard/product/sell" component={Sell}/>
                                    <Route path="/dashboard/product/sells" component={SellsReport}/>
                                    <Route path="/dashboard/product/sell-details/:id" component={SellDetails}/>
                                    <Route path="/dashboard/product/add" component={AddProducts}/>
                                    <Route path="/dashboard/product/edit/:id" component={AddProducts}/>
                                    <Route path="/dashboard/customers" component={Customers}/>
                                    <Route path="/dashboard/customer/details/:id" component={CustomerDetails}/>
                                    <Route path="/dashboard/customer/add" component={AddCustomer}/>
                                    <Route path="/dashboard/customer/edit/:id" component={AddCustomer}/>
                                    <Route path="/dashboard/profile" component={Profile}/>
                                </Switch>
                            </div>
                        </main>
                        <Footer/>
                    </div>
                </div>
            </div>

        </div>

    );
};

export default Dashboard;
