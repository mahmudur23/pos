import React from 'react';
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faAngleDown,
    faBookOpen,
    faChartArea,
    faTable,
    faTachometerAlt,
    faCog,
} from "@fortawesome/free-solid-svg-icons";

const Sidebar = () => {
    return (
        <div id="layoutSidenav_nav">
            <nav className="sb-sidenav accordion sb-sidenav-dark card" style={{borderRadius: 0}} id="sidenavAccordion">
                <div className="sb-sidenav-menu">
                    <div className="nav">
                        <div className="sb-sidenav-menu-heading">Core</div>
                        <Link className="nav-link" to="/dashboard/home">
                            <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faTachometerAlt}/></div>
                            Dashboard
                        </Link>
                        <div className="sb-sidenav-menu-heading">Interface</div>
                        <a className="nav-link collapsed" href="#" data-bs-toggle="collapse"
                           data-bs-target="#collapseLayouts" aria-expanded="false"
                           aria-controls="collapseLayouts">
                            <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faCog}/></div>
                            Settings
                            <div className="sb-sidenav-collapse-arrow"><FontAwesomeIcon icon={faAngleDown}/>
                            </div>
                        </a>
                        <div className="collapse" id="collapseLayouts" aria-labelledby="headingOne"
                             data-bs-parent="#sidenavAccordion">
                            <nav className="sb-sidenav-menu-nested nav">
                                <a className="nav-link" href="#">Static Navigation</a>
                                <a className="nav-link" href="#">Light Sidenav</a>
                            </nav>
                        </div>
                        <a className="nav-link collapsed" href="#" data-bs-toggle="collapse"
                           data-bs-target="#collapsePages" aria-expanded="false"
                           aria-controls="collapsePages">
                            <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faBookOpen}/></div>
                            Pages
                            <div className="sb-sidenav-collapse-arrow"><FontAwesomeIcon icon={faAngleDown}/>
                            </div>
                        </a>
                        <div className="collapse" id="collapsePages" aria-labelledby="headingTwo"
                             data-bs-parent="#sidenavAccordion">
                            <nav className="sb-sidenav-menu-nested nav accordion"
                                 id="sidenavAccordionPages">
                                <a className="nav-link collapsed" href="#" data-bs-toggle="collapse"
                                   data-bs-target="#pagesCollapseAuth" aria-expanded="false"
                                   aria-controls="pagesCollapseAuth">
                                    Products
                                    <div className="sb-sidenav-collapse-arrow"><FontAwesomeIcon icon={faAngleDown}/></div>
                                </a>
                                <div className="collapse" id="pagesCollapseAuth"
                                     aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                    <nav className="sb-sidenav-menu-nested nav">
                                        <Link className="nav-link" to="/dashboard/products">All Products</Link>
                                        <Link className="nav-link" to="/dashboard/product/code">Products Code</Link>
                                        <Link className="nav-link" to="/dashboard/product/category">Products Category</Link>
                                        <Link className="nav-link" to="/dashboard/product/sell">Sell</Link>
                                        <Link className="nav-link" to="/dashboard/product/sells">Sells List</Link>
                                    </nav>
                                </div>
                                <a className="nav-link collapsed" href="#" data-bs-toggle="collapse"
                                   data-bs-target="#pagesCollapseError" aria-expanded="false"
                                   aria-controls="pagesCollapseError">
                                    Customers
                                    <div className="sb-sidenav-collapse-arrow"><FontAwesomeIcon icon={faAngleDown}/></div>
                                </a>
                                <div className="collapse" id="pagesCollapseError"
                                     aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                    <nav className="sb-sidenav-menu-nested nav">
                                        <Link className="nav-link" to="/dashboard/customers">All Customers</Link>
                                    </nav>
                                </div>
                            </nav>
                        </div>
                        <div className="sb-sidenav-menu-heading">Addons</div>
                        <a className="nav-link" href="#">
                            <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faChartArea}/></div>
                            Charts
                        </a>
                        <a className="nav-link" href="#">
                            <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faTable}/></div>
                            Tables
                        </a>
                    </div>
                </div>
                <div className="sb-sidenav-footer">
                    <div className="small">Logged in as:</div>
                    Admin
                </div>
            </nav>
        </div>
    );
};

export default Sidebar;
