// actions index

export const login = (data) => {
    return {
        type: "LOGIN",
        payload: data
    }
};

export const pageAction = (data) => {
    return {
        type: "PAGE",
        payload: data
    }
};
